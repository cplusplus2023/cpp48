 ///
 /// @file    3_Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 10:21:04
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	//当我们没有显式定义构造函数时，
	//系统会自动提供一个默认的构造函数
#if 1
	//我们自己显式定义的一个无参构造函数
	Point()
	{
		cout << "Point()" << endl;
		_ix = 0;
		_iy = 0;
	}
#endif

	void print() 
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")" << endl;
	}

private:
	int _ix;
	int _iy;
};


 
void test0() 
{
	Point pt;
	pt.print();

	/* Point pt2(1, 2); */
	/* cout << "pt2:"; */
	/* pt2.print(); */
} 
 
int main(void)
{
	test0();
	return 0;
}
