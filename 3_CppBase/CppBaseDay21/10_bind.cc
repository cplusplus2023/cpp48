#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

//普通函数(非成员函数)
int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}

int func(int x, int y, int z)
{
    cout << "int func(int, int, int)" << endl;
    return x * y * z;
}

class Example
{
public:
    int add(int x, int y)
    {
        cout << "int Example::add(int, int)" << endl;
        return x + y;
    }

};

void test()
{
    auto f = bind(&add, 1, 2);
    cout << "f() = " << f() << endl;

    auto f2 = bind(&func, 1, 3, 5);
    cout << "f2() = " << f2() << endl;

    Example ex;
    auto f3 = bind(&Example::add, &ex, 10, 20);
    cout << "f3() = " << f3() << endl;

    using namespace std::placeholders;//占位符
    auto f4 = bind(add, 1, _1);
    cout << "f4(9) = " << f4(9) << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

