#include <signal.h>
#include <iostream>
#include <string>
#include <vector>

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>
#include <workflow/RedisMessage.h>

using std::cout;
using std::endl;
using std::string;
using std::vector;

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    cout << "sighandler is processing" << endl;
    waitGroup.done();
}

void redisCallback(WFRedisTask * redisTask)
{
    cout << "redisCallback is running" << endl;

    int * pNumber = static_cast<int*>(redisTask->user_data);
    cout << "task *pNumber:" << *pNumber << endl;

    //错误检测
    auto req = redisTask->get_req();
    auto resp = redisTask->get_resp();
    int state = redisTask->get_state();
    int error = redisTask->get_error();
    protocol::RedisValue result;//结果集
    switch(state){
    case  WFT_STATE_SYS_ERROR:
        printf("system error: %s\n", strerror(error));break;
    case  WFT_STATE_DNS_ERROR:
        printf("dns error: %s", gai_strerror(state)); break;
    case WFT_STATE_SUCCESS:
        resp->get_result(result);
        break;
    }
    if(state != WFT_STATE_SUCCESS) {
        printf("occurs error\n");
        return;
    }

    //接下来就是解析RedisRequest
    string command;
    vector<string> params;
    req->get_command(command);
    req->get_params(params);
    cout << "Request command：" << command << " ";
    for(auto & elem : params) {
        cout << elem << " ";
    }
    cout << endl;


    //解析响应信息RedisResponse.RedisValue
    if(result.is_int()) {
        cout << "Response is int:" << result.int_value() << endl << endl;
        //在这里创建第二个任务
        string url("redis://127.0.0.1:6379");
        auto redisTask2 = WFTaskFactory::create_redis_task(url, 0, redisCallback);
        
        //将任务1的user_data的指针转交给任务2
        redisTask2->user_data = redisTask->user_data;

        string command("hgetall");
        vector<string> params{"student"};
        redisTask2->get_req()->set_request(command, params);
        //通过第一个任务来获取序列, 在其中添加第二个任务
        series_of(redisTask)->push_back(redisTask2);
    } else if(result.is_array()) {
        cout << "Response is array" << endl;
        for(size_t i = 0; i < result.arr_size(); ++i) {
            cout << "arr[" << i << "]:" 
                 << result.arr_at(i).string_value() << endl;
        }
    }
}

void test0()
{
    signal(SIGINT, sighandler);

    int number = 10;

    //1. 创建任务
    auto redisTask = WFTaskFactory::create_redis_task(
                        "redis://127.0.0.1:6379", 0, redisCallback);
    //将主线程中的数据number交给任务1
    redisTask->user_data = (void*)&number;

    //2. 设置任务的属性
    auto req = redisTask->get_req();
    string command("hset");
    vector<string> params{"student", "name", "liubei", "age", "40"};
    req->set_request(command, params);

    //3. 交给框架调度执行
    redisTask->start();

    waitGroup.wait();
}


int main()
{
    test0();
    return 0;
}

