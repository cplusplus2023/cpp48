#include "mq.h"
#include "oss.h"
#include <iostream>

#include <nlohmann/json.hpp>
using std::cout;
using std::endl;
using Json = nlohmann::json;

void test0()
{
    AmqpInfo amqpinfo;
    MessageConsumer consumer(amqpinfo);
    OssUploader ossuploader;
    while(true) {
        //1. 从消息队列中获取消息
        string fileInfo;
        bool hasMsg = consumer.doConsume(fileInfo);
        if(hasMsg) {
            //2. 解析JSONS对象
            Json uploadFileInfo = Json::parse(fileInfo);
            string filepath = uploadFileInfo["filepath"];
            string objectName = uploadFileInfo["objectName"];
            //3. 执行备份操作，上传到OSS
            ossuploader.doUpload(objectName, filepath);
        }
    }
}

int main()
{
    test0();
    return 0;
}

