#ifndef __EVENTFD_H__
#define __EVENTFD_H__

#include <functional>

using std::function;

class EventFd
{
    using EventFdCallback = function<void()>;
public:
    EventFd(EventFdCallback &&cb);
    ~EventFd();

    void start();
    void stop();

    void handleRead();
    void wakeup();

    int createEventFd();

private:
    int _evtfd;//eventfd返回的文件描述符
    bool _isStarted;//标志是否运行
    EventFdCallback _cb;//需要执行的任务
    

};

#endif
