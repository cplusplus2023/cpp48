 ///
 /// @file    constructVirtualFunc.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-04 11:26:28
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Grandpa
{
public:
	Grandpa()
	: _data1(1)
	{
		cout << "Grandpa()" << endl;
		cout << "_data1:" << this->_data1 << endl;
	}

	~Grandpa() {
		cout << "~Grandpa()" << endl;
	}

	virtual void print() const
	{	cout << "Grandpa::print()" << endl;	}

	virtual void display() const
	{	cout << "Grandpa::display()" << endl;	}

private:
	long _data1;
};

class Parent
: public Grandpa
{
public:
	Parent() 
	: _data2(2)
	{
		cout << "Parent()" << endl;
		//当调用Parent构造函数时，并不知道有Son的存在
		this->print();//采用静态联编（静态多态）
		cout << "_data2:" << this->_data2 << endl;
	}

	~Parent() {
		cout << "Parent()" << endl;
		//当执行到Parent的析构函数时，派生类部分已经不存在了
		//因此也只能调用本层及以上的虚函数
		//只能体现出静态联编（静态多态）
		display();
	}

#if 1
	virtual void print() const override
	{	cout << "Parent::print()" << endl;	}

	virtual void display() const override
	{	cout << "Parent::display()" << endl;	}
#endif

private:
	long _data2;
};

class Son
: public Parent
{
public:
	Son() 
	: _data3(3)
	{	
		cout << "Son()" << endl;
		cout << "_data3:" << this->_data3 << endl;
	}
	~Son() {	cout << "~Son()" << endl;	}

	virtual void print() const override
	{	cout << "Son::print()" << endl;	}

	virtual void display() const override
	{	cout << "Son::display()" << endl;	}

private:
	long _data3;
};
 
void test0() 
{	//先要创建对象，对象没有创建完毕，无法体现动态多态特性
	Son son;
} 
 
int main(void)
{
	test0();
	return 0;
}
