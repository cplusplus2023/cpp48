#include <48func.h>
int main()
{
    write(STDOUT_FILENO,"hello\n",6); // 相当于 printf("hello\n");
    write(STDERR_FILENO,"error\n",6); // 相当于 fprintf(stderr,"error\n");
    char buf[1024] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n",buf); // 相当于 fgets(buf,sizeof(buf),stdin);
    return 0;
}

