 ///
 /// @file    io3.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 17:07:33
 ///
 
#include <string.h>

#include <vector>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;
using std::fstream;

 
void test0() 
{
	//打开文件流
	//文件必须存在；否则流是无效的
	ifstream ifs("io2.cc");
	if(!ifs.good()) {
		cout << "ifstream open file error" << endl;
		return;
	}

	string str;
	//输入流运算符默认情况下是用空格 \t 进行分割
	while(ifs >> str) {
		cout << str << endl;
	}

	//关闭文件流
	ifs.close();
} 
 
void test1() 
{
	//打开文件流
	//文件必须存在；否则流是无效的
	ifstream ifs("io2.cc");
	if(!ifs.good()) {
		cout << "ifstream open file error" << endl;
		return;
	}

	//每次获取一行数据
	//通过流对象自带的getline方法完成的,
	//具有局限性
	char buff[1024] = {0};
	while(ifs.getline(buff, sizeof(buff))) {
		cout << buff << endl;
	}

	//关闭文件流
	ifs.close();
} 

void test2() 
{
	//打开文件流
	//文件必须存在；否则流是无效的
	ifstream ifs("io2.cc");
	if(!ifs.good()) {
		cout << "ifstream open file error" << endl;
		return;
	}

	vector<string> fileVec;
	//每次获取一行数据, 换行符不会写入line中
	string line;
	while(std::getline(ifs, line)) {
		fileVec.push_back(line);
	}

	for(auto & elem : fileVec) {
		cout << elem << endl;
	}

	//关闭文件流
	ifs.close();
} 

void test3()
{
	//文件输出流不要求文件必须存在；
	//当没有文件时，会新建一个文件
	//当文件存在时，会清空文件的内容
	ofstream ofs("test.txt");
	if(!ofs) {
		cout << "ofstream open file error!" << endl;
		return;
	}

	ofs.close();
}

void test4()
{
	ofstream ofs("test.txt", std::ios::app);
	if(!ofs) {
		cout << "ofstream open file error!" << endl;
		return;
	}

	string line("this is a new line\n");
	ofs << line;

	const char * pstr = "hello,world";
	ofs.write(pstr, strlen(pstr));

	ofs.close();
}

void test5()
{
	ifstream ifs("io2.cc");
	if(!ifs.good()) {
		cout << "ifstream open file error" << endl;
		return;
	}

	//获取文件的长度
	ifs.seekg(0, std::ios::end);//偏移到文件的末尾
	size_t length = ifs.tellg();//获取当前偏移位置信息
	cout << "length:" << length << endl;

	char * pbuf = new char[length + 1]();
	ifs.seekg(0, std::ios::beg);//偏移到文件的起始位置
	ifs.read(pbuf, length);//读取指定的字符长度

	string content(pbuf);
	cout << "content:" << content << endl;
	delete [] pbuf;

	//关闭文件流
	ifs.close();
}

void test6()
{
	//先从终端获取5个整型数据，写入文件；
	//再从该文件中读出这5个整型数据
	fstream fs;//要求文件必须存在；否则出错
	fs.open("test2.txt");
	if(!fs) {
		cout << "fstream open error!" << endl;
		return;
	}

	int number = 0;
	for(int i = 0; i < 5; ++i) {
		std::cin >> number;
		fs << number << " ";
	}

	size_t pos = fs.tellp();
	cout << "pos:" << pos << endl;
	fs.seekp(0);//偏移到文件开始的位置

	for(int i = 0; i < 5; ++i) {
		fs >> number;
		cout << number << " ";
	}
	cout << endl;

	fs.close();
}




int main(void)
{
	/* test0(); */
	/* test1(); */
	/* test2(); */
	/* test3(); */
	/* test4(); */
	/* test5(); */
	test6();
	return 0;
}
