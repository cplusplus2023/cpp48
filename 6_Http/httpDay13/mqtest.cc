
#include <string>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

#include <SimpleAmqpClient/SimpleAmqpClient.h>
using namespace AmqpClient;

struct AmqpInfo{
    string url = "amqp://guest:guest@localhost:5672";
    string exchanger = "uploadserver.trans";
    string ossqueue = "uploadserver.trans.oss";
    string routingkey = "oss";
};



void publish_test()
{
    AmqpInfo amqpinfo;
    //创建一个channel
    AmqpClient::Channel::ptr_t channel = AmqpClient::Channel::Create();
    AmqpClient::BasicMessage::ptr_t message = AmqpClient::BasicMessage::Create("hello");
    channel->BasicPublish(amqpinfo.exchanger, amqpinfo.routingkey, message);
    /* pause(); */
}

void consume_test()
{
    AmqpInfo amqpinfo;
    AmqpClient::Channel::ptr_t channel = AmqpClient::Channel::Create();
    //启动一个消费者队列
    channel->BasicConsume(amqpinfo.ossqueue);
    AmqpClient::Envelope::ptr_t envelope;
    //取消息
    bool flag = channel->BasicConsumeMessage(envelope, 3000);
    if(flag == false) {
        cout << "get message timeout" << endl;
        return;
    }
    cout << "message: " << envelope->Message()->Body() << endl;
}

int main()
{
    /* publish_test(); */
    consume_test();
    return 0;
}



