 ///
 /// @file    typeCast1.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 10:19:24
 ///
 
#include <iostream>
using std::cout;
using std::endl;


class Complex
{
public:
	Complex(double dreal = 0, double dimage = 0)
	: _dreal(dreal)
	, _dimage(dimage)
	{	cout << "Complex(double,double)" << endl;}

	friend std::ostream & operator<<(std::ostream & os, const Complex & rhs);
	friend class Point;

private:
	double _dreal;
	double _dimage;
};

std::ostream & operator<<(std::ostream & os, const Complex & rhs)
{
	os << rhs._dreal;
	if(rhs._dimage > 0) {
		os << " + " << rhs._dimage << "i" << endl;
	} else if(rhs._dimage == 0){
		return os;
	} else {
		os << " - " <<  rhs._dimage * ( -1 )  << "i" << endl;
	}
	return os;
}

class Point
{
public:
	//有可能发生隐式转换的情况是只有一个参数的构造函数
	//不希望隐式转换发生
	//explicit
	Point(int ix)
	: _ix(ix)
	, _iy(0)
	{	cout << "Point(int)" << endl;	}

	Point(const Point & rhs)
	: _ix(rhs._ix)
	, _iy(rhs._iy)
	{	cout << "Point(const Point&)" << endl;}


	Point & operator=(const Point & rhs)
	{
		cout << "Point & operator=(const Point&)" << endl;
		_ix = rhs._ix;
		_iy = rhs._iy;
		return *this;
	}

	//提供了一个重载形式
	Point & operator=(const Complex & rhs)
	{
		cout << "Point & operator=(const Complex &)" << endl;
		_ix = rhs._dreal;
		_iy = rhs._dimage;
		return *this;
	}

	Point(int ix, int iy)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Point(int,int)" << endl;}

	friend std::ostream & operator<<(std::ostream & os, const Point & rhs);

	~Point() {	cout << "~Point()" << endl;	}

private:
	int _ix;
	int _iy;
};
	
std::ostream & operator<<(std::ostream & os, const Point & rhs)
{
	os << "(" << rhs._ix
	   << "," << rhs._iy
	   << ")";
	return os;
}
 
void test0() 
{
	Point pt = 1;// 1 ==> Point(1, 0) ==> pt 隐式转换
	cout << "pt:" << pt << endl;

	Complex c1(10, 11);

	pt = c1;//c1 ==> Point ==> pt
	cout << "pt:" << pt << endl;
 
} 
 
int main(void)
{
	test0();
	return 0;
}
