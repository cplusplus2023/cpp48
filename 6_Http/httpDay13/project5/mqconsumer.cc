#include "oss.h"
#include "mq.h"
#include <iostream>

#include <nlohmann/json.hpp>
#include <alibabacloud/oss/OssClient.h>
using std::cout;
using std::endl;
using std::string;

void test0()
{
    OssUploader ossuploader;
    AmqpInfo mqinfo;
    MessageConsumer consumer(mqinfo);
    while(true) {
        string fileinfo;
        bool hasMsg = consumer.doConsume(fileinfo);
        if(hasMsg) {
            using Json = nlohmann::json;
            Json fileJson = Json::parse(fileinfo);
            string filepath = fileJson["filepath"];
            string objectName = fileJson["objectName"];
            ossuploader.doUpload(objectName, filepath);
        }   
    }
}


int main()
{
    test0();
    return 0;
}

