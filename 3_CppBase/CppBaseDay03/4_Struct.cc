 ///
 /// @file    Computer.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 18:05:03
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//在C++中，struct的功能已经被扩展了，
//他相当于就是一个类
struct Computer
{
//struct的默认访问权限是public的
public:
	Computer(const char * brand, float price)
	: _brand(new char[strlen(brand) + 1]())
	, _price(price)
	{
		cout << "Computer(const char*, float)" << endl;
		strcpy(_brand, brand);
	}

	void print();

	//系统自动提供的析构函数无法解决问题了
	~Computer()
	{
		cout << "~Computer()" << endl;
		if(_brand) {
			//Safe delete操作
			delete [] _brand;
			_brand = nullptr;
		}
	}

private:
	char * _brand;
	float _price;
};
 

void Computer::print() 
{
	cout << "brand:" << _brand << endl;
	cout << "price:" << _price << endl;
}

void test0() 
{
	cout << "sizeof(Computer):"<< sizeof(Computer) << endl;
	Computer pc("Xiaomi", 7777);//pc称为对象
	pc.print();
	cout << pc._price << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
