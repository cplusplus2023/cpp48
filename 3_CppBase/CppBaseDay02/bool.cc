 ///
 /// @file    bool.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 17:19:27
 ///
 
#include <iostream>
using std::cout;
using std::endl;
 
void test0() 
{
	bool b1 = 100;
	bool b2 = -100;
	bool b3 = 1;
	bool b4 = 0;
	bool b5 = true;
	bool b6 = false;

	cout << "sizeof(bool): " << sizeof(bool) << endl;
	cout << "b1:" << b1 << endl;
	cout << "b2:" << b2 << endl;
	cout << "b3:" << b3 << endl;
	cout << "b4:" << b4 << endl;
	cout << "b5:" << b5 << endl;
	cout << "b6:" << b6 << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
