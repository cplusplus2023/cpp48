 ///
 /// @file    Stack.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 09:42:50
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Stack
{
public:
	Stack(int size = 10)
	: _top(-1)
	, _size(size)
	, _data(new int[_size]())
	{}

	bool empty() const
	{	return _top == -1;	}

	bool full() const
	{	return _top == _size - 1;	}

	void push(int num) {
		if(!full()) {
			_data[++_top] = num;
		} else {
			cout << "stack is full, cannnot push any more data!" << endl;
		}
	}

	void pop() {
		if(!empty()) {
			--_top;
		} else {
			cout << "stack is empty!no more data!" << endl;
		}
	}

	int top() const 
	{
		return _data[_top];
	}

	~Stack() {
		if(_data) {
			delete [] _data;
			_data = nullptr;
		}
	}

private:
	int _top;
	int _size;
	int * _data;
};
 
void test0() 
{
	Stack stack;
	cout << "栈是否为空?" << stack.empty() << endl;
	stack.push(1);
	cout << "栈是否为空?" << stack.empty() << endl;

	for(int i = 2; i < 12; ++i) {
		stack.push(i);
	}
	cout << "栈是否已满?" << stack.full() << endl;

	while(!stack.empty()) {
		cout << stack.top() << endl;
		stack.pop();
	}
	cout << "栈是否为空?" << stack.empty() << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
