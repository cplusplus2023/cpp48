 ///
 /// @file    exception.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 17:23:23
 ///
 
#include <iostream>
using std::cout;
using std::endl;

double division(double x, double y)
{
	if(y == 0) {
		throw "divide cannot be zero!!";
	}else {
		return x / y;
	}
}
 
void test0() 
{
	cout << division(11.11, 2) << endl;
	cout << division(1, 0) << endl;
} 

void test1()
{	//try-catch的写法不推荐使用
	//google的编程规范之中明确规定：不能使用C++异常
	//因为了它破坏了代码结构, 不够优雅
    double x, y;
	std::cin >> x >> y;
    try { //尝试去运行try子句
        if(y == 0)
            throw "divide cannot be zero!";
        else
            cout << (x / y) << endl; 
    } catch(double d) {//捕获子句
		//对异常行为进行处理
        cout << "catch(double)" << endl;
    } catch(int e) {
        cout << "catch(int)" << endl;
    } catch(const char * p) {
		cout << p << endl;
	}
}

int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
