#include <iostream>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::vector;
using std::ostream_iterator;

void test()
{
    vector<int> number;
    number.push_back(1);

    bool flag = true;
    for(auto it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
        if(flag)
        {
            number.push_back(2);//底层已经发生了扩容现象,迭代器失效
            flag = false;
            it = number.begin();
        }
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

