#include <48func.h>
void *threadFunc(void *arg){
    printf("Hello\n");
    sleep(5);
    printf("World\n");
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    pthread_join(tid,NULL);
    printf("I am main!\n");
    return 0;
}

