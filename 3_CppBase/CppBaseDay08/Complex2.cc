 ///
 /// @file    Complex.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 10:04:17
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//数学之中的复数, 负数怎么开方   -1 = i^2
class Complex
{
public:
	Complex(double dreal = 0, double dimage = 0)
	: _dreal(dreal)
	, _dimage(dimage)
	{	cout << "Complex(double,double)" << endl;}

	void print() const
	{
		cout << _dreal;
		if(_dimage > 0) {
			cout << " + " << _dimage << "i" << endl;
		} else if(_dimage == 0){
			return;
		} else {
			cout << " - " <<  _dimage * ( -1 )  << "i" << endl;
		}
	}

	//非静态成员函数第一个参数是this指针，
	//代表的就是左操作数
	Complex operator+(const Complex & rhs)
	{
		cout << "Complex operator+(const Complex &)" << endl;
		return Complex(this->_dreal + rhs._dreal,
					   this->_dimage + rhs._dimage);
	}

private:
	double _dreal;
	double _dimage;
};

//重载的类型必须是一个自定义类类型或枚举类型
//不能对内置类型的运算符进行重载, 编译报错
//int operator+(int x, int y)
//{	return x - y;}

 
void test0() 
{
	Complex c1(1, 2), c2(10, -3);
	Complex c3 = c1 + c2;
	cout << "c3:";
	c3.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
