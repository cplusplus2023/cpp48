#include <48func.h>
int main()
{
    int pipefds[2];
    pipe(pipefds);
    if(fork() == 0){
        // close(pipefds[0]);
        // char buf[1024] = {0};
        // //ssize_t sret = read(pipefds[0],buf,sizeof(buf));
        // //printf("sret = %ld, buf = %s\n",sret, buf);
        // int cnt = 0;
        // while(1){
        //     ssize_t sret = write(pipefds[1],buf,1024);
        //     printf("sret = %ld, cnt = %d\n", sret, cnt++);
        //     sleep(1);
        close(pipefds[1]);
        char buf[1024] = {0};
        while(1){
            ssize_t sret = read(pipefds[0],buf,256);
            printf("sret = %ld, buf = %s\n",sret, buf);
            sleep(1);
        }
    }
    else{
       // sleep(5);
       // close(pipefds[0]);
       // printf("read side is closed\n");
        close(pipefds[0]);
        char buf[1024] = {0};
        write(pipefds[1],buf,1024);
        sleep(5);
        close(pipefds[1]);
        printf("write side is closed\n");
        int wstatus;
        wait(&wstatus);
        if(WIFEXITED(wstatus)){
            printf("Normal exit! return value = %d\n", WEXITSTATUS(wstatus));
        }
        else if(WIFSIGNALED(wstatus)){
            printf("Been signaled! terminal signal = %d\n", WTERMSIG(wstatus));
        }
    }
    }

