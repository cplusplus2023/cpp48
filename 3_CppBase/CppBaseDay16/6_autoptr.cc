#include <memory> //vimplus快捷键  ,o
#include <iostream>
using std::cout;
using std::endl;
using std::auto_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    ~Point() {  cout << "~Point()" << endl; }

    void print() const
    {   cout << "(" << _ix << "," << _iy << ")" << endl;}

private:
    int _ix;
    int _iy;
};  

void test0()
{
    Point * pt = new Point(1, 2);
    auto_ptr<Point> ap(pt);
    ap->print();
    (*ap).print();
    cout << "ap.get(): " << ap.get() << endl;
    cout << "pt: " << pt << endl;

    //从语法形式上来看,这是一个拷贝构造
    //那么ap就应该是保持不变的
    //但在底层已经发生了所有权的转移,
    //表达的是移动语义,并不是值语义
    //因此出现了矛盾,存在缺陷,所以被弃用了
    auto_ptr<Point> ap2(ap);
    //ap->print();//error 段错误
    cout << "ap2->print(): " << endl;
    ap2->print();
    //ap2托管的就是ap托管的资源
    cout << "ap2.get(): " << ap2.get() << endl;
}


int main()
{
    test0();
    return 0;
}










