 ///
 /// @file    add.hpp
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 16:43:39
 ///
 
#ifndef __ADD_HPP__
#define __ADD_HPP__

inline 
int add(int x, int y);
 
 
 
#endif
