#include "user.pb.h"

#include <iostream>
using std::cout;
using std::endl;
using std::string;

void test0()
{
    ReqSignup req;
    req.set_username("admin");
    req.set_password("123");
    string bytes;
    req.SerializeToString(&bytes);
    for(size_t i = 0; i < bytes.size(); ++i) {
        printf("%02x", bytes[i]);
    }
    printf("\n");

}


int main()
{
    test0();
    return 0;
}

