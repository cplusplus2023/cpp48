#include <48func.h>
void handler(int signum){
    printf("before signum = %d\n", signum);
    sleep(5);
    sigset_t set;
    sigpending(&set);
    if(sigismember(&set,SIGINT)){
        printf("SIGINT is pending!\n");
    }
    else{
        printf("SIGINT is not pending!\n");
    }
    if(sigismember(&set,SIGQUIT)){
        printf("SIGQUIT is pending!\n");
    }
    else{
        printf("SIGQUIT is not pending!\n");
    }
    printf("after signum = %d\n", signum);
}
int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act,0,sizeof(act));
    act.sa_handler = handler;
    act.sa_flags = SA_RESTART;
    // 处理额外临时屏蔽集合
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask,SIGQUIT);
    sigaction(SIGINT,&act,NULL);
    char buf[1024] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

