#include <signal.h>
#include <iostream>

#include <workflow/WFFacilities.h>

using std::cout;
using std::endl;

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    cout << "sighandler is processing" << endl;
    waitGroup.done();
}

void timerCallback(WFTimerTask * timerTask)
{
    cout << "timerCallback is running" << endl;
    //to-do something;
    //周期性的调用
    auto timertask = WFTaskFactory::create_timer_task(3 * 1000 * 1000, timerCallback);
    series_of(timerTask)->push_back(timertask);
}

void test0()
{
    signal(SIGINT, sighandler);

    auto timerTask = WFTaskFactory::create_timer_task(3 * 1000 * 1000, timerCallback);
    timerTask->start();
    waitGroup.wait();
}


int main()
{
    test0();
    return 0;
}

