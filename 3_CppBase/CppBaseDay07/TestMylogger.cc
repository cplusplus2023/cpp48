 ///
 /// @file    TestMylogger.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-26 11:12:17
 ///
 
#include "Mylogger.hpp"

#include <sstream>
#include <iostream>
using std::cout;
using std::endl;
using std::string;
 
using namespace wd;
void test0() 
{
	Mylogger::getInstance()->error("this is an error message");
	Mylogger::getInstance()->warn("this is a warn message");
	Mylogger::getInstance()->info("this is an info message");
	Mylogger::getInstance()->debug("this is a debug message");
} 

void test1()
{
	cout << __FILE__ << endl;
	cout << __FUNCTION__ << endl;
	cout << __LINE__ << endl;
}
//[file:func:line]

int test2()
{
	Mylogger::getInstance()->error(addprefix("this is an error message"));
	Mylogger::getInstance()->warn(addprefix("this is a warn message"));
	Mylogger::getInstance()->info(addprefix("this is an info message"));
	Mylogger::getInstance()->debug(addprefix("this is a debug message"));
}

void test3()
{
	LogError("this is an error message");
	LogWarn("this is a warn message");
	LogInfo("this is an info message");
	LogDebug("this is a debug message");

	Mylogger::destroy();
}



int main(void)
{
	/* test0(); */
	/* test1(); */
	/* test2(); */
	test3();
	return 0;
}
