 ///
 /// @file    add.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 16:20:20
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//宏函数的作用是形式上像函数，但并不是函数，
//在预处理时，会直接进行字符串的替换
//
//由于宏函数没有类型检查，当出现错误时，会延迟到运行时
#define MAX(x,y)  ((x) > (y) ? (x) : (y))


//inline函数
//作用是: 在编译时，如果发现参数的调用是没有问题的，
//会直接进行函数体的替换, 也是没有函数的调用开销的
//
//在编译时会有类型检查，相比于宏函数会更安全
//
//相比于宏函数而言，可以提前发现错误的
inline 
int max(int x, int y);

//对于inline函数有一些限定:
//1. 函数体本身很长的情况，是不适合设置为inline函数，会导致代码膨胀
//2. 函数体内有循环执行体，也不适合设置为Inline函数
 
void test0() 
{
	int a = 3, b = 4;
	//cout << MAX(a, b++) << endl;// (a) > (b++) ?(a):(b++);
	//使用宏函数的预期，b的值执行完毕之后应该是5
	//cout << "b:" << b << endl;
	/* cout << MAX(a++, b) << endl; */
	//表达式的值: b++   后置++， 返回的是b变化之前的值
	//当表达式b++执行结束之后，b的值会加1
	cout << max(a, b++) << endl;
	cout << "b:" << b << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}

//函数实现（定义）
inline
int max(int x, int y)
{	
	return x > y ? x : y;
}

