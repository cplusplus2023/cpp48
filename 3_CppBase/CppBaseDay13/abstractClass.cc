 ///
 /// @file    abstractClass.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-05 10:30:13
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//基类Base不能实例化对象
//但派生类Derived可以实例化对象
//
//定义了protected型构造函数的类也称为抽象类
class Base {
protected:
	Base() {	cout << "Base()" << endl;	}	

private:
	long _base;
};

class Derived
: public Base {
public:
	Derived()
	: Base()
	//, _b()
	{	cout << "Derived()" << endl;	}

private:
	//Base _b;
	long _derived;
};
 
void test0() 
{
	//Base base;//errror,不能实例化对象
	Derived derived;
} 
 
int main(void)
{
	test0();
	return 0;
}
