 ///
 /// @file    io2.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 16:39:42
 ///
 
#include <unistd.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
 
void test0() 
{
	cout << "hello";
	cout << "world\n";
	sleep(3);
} 

void test1()
{
	//cout的缓冲区中最多能存放1024个字节
	//当缓冲区满时，会直接输出缓冲区中的数据
	for(int i = 0; i < 1025; ++i) {
		cout << 'a';
	}
	sleep(3);
}

void test2()
{
	cout << "hello";
	cerr << "world";
}
 
int main(void)
{
	/* test0(); */
	/* test1(); */
	test2();
	return 0;
}
