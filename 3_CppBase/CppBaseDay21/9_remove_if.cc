#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::copy;
using std::count;
using std::vector;
using std::ostream_iterator;

void test()
{
    vector<int> number = {1, 3, 5, 6, 5, 7, 8, 9, 2, 4};
    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl << "执行remove_if函数" << endl;
    /* auto it = remove_if(number.begin(), number.end(), */ 
    /*                     std::bind1st(std::less<int>(), 5)); */
    auto it = remove_if(number.begin(), number.end(), 
                        std::bind2nd(std::greater<int>(), 5));

    /* auto it = remove_if(number.begin(), number.end(), [](int value){ */
    /*                     return value > 5; */
    /*                     }); */
    number.erase(it, number.end());
    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

