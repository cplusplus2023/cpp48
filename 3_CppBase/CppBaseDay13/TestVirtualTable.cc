 ///
 /// @file    TestVirtualTable.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-05 11:23:54
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
	Base(long b1)
	: _b1(b1)
	{	cout << "Base(long)" << endl;	}

	virtual void func1() {	cout << "Base::func1()" << endl;	}
	virtual void func2() {	cout << "Base::func2()" << endl;	}
	virtual void func3() {	cout << "Base::func3()" << endl;	}

private:
	long _b1;
};

class Derived
: public Base
{
public:
	Derived(long b1, long d1)
	: Base(b1)
	, _d1(d1)
	{	cout << "Derived(long, long)" << endl;	}	

	virtual void func1() 
	{	
		cout << "Derived::func1()" << _d1 << endl;
		
	}
	virtual void func2() {	cout << "Derived::func2()" << endl;	}
	virtual void func3() {	cout << "Derived::func3()" << endl;	}

private:
	long _d1;
};
 
void test0() 
{
	typedef void(*Function)();

	Derived d(10, 100);

	long ** p = (long**)&d;
	long * pvtable = p[0];
	cout << (long)p[1] << endl;
	cout << (long)p[2] << endl;

	long f1 = pvtable[0];
	long f2 = pvtable[1];
	long f3 = pvtable[2];
	cout << "f1:" << f1 << endl
		 << "f2:" << f2 << endl
		 << "f3:" << f3 << endl;

	Function f0 = (Function)f1;
	f0();

	f0 = (Function)f2;
	f0();

	f0 = (Function)f3;
	f0();

} 
 
int main(void)
{
	test0();
	return 0;
}
