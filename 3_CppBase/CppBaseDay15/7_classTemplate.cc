#include <vector>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::string;
using std::vector;


//类模板的写法:

//默认参数只需要在类前设置即可,
//在类之外定义的成员函数前不需要再设置
template <class T, int kSize = 10>
class Stack
{
public:
    Stack()
    : _top(-1)
    , _data(new T[kSize]())
    {}

    ~Stack();

    bool empty() const;
    bool full() const;
    void push(const T & t);
    void pop();
    T & top();
private:
    int _top;
    T * _data;
};

//在类之外定义成员函数时，需要加上模板参数列表
template <class T, int kSize>
Stack<T, kSize>::~Stack()
{
    if(_data) {
        delete [] _data;
        _data = nullptr;
    }
}

template <class T, int kSize>
bool Stack<T, kSize>::empty() const
{   return _top == -1;  }

template <class T, int kSize>
bool Stack<T, kSize>::full() const
{   return _top == kSize - 1;   }


template <class T, int kSize>
void Stack<T, kSize>::push(const T & t)
{
    if(!full()) {
        _data[++_top] = t;
    } else 
    {
        cout << "stack is full, cannot push any more data" << endl;
    }
}

template <class T, int kSize>
void Stack<T, kSize>::pop()
{
    if(!empty()) {
        --_top;
    }else {
        cout << "stack is empty, no more data" << endl;
    }
}

template <class T, int kSize>
T & Stack<T, kSize>::top()
{
    return _data[_top];
}

void test0()
{
    vector<int> numbers;

    Stack<int> stack;
    cout << "此时栈是否为空?" << stack.empty() << endl;
    stack.push(1);
    cout << "此时栈是否为空?" << stack.empty() << endl;

    for(int i = 2; i < 12; ++i) {
        stack.push(i);
    }
    cout << "此时栈是否已满?" << stack.full() << endl;
    while(!stack.empty()) {
        cout << stack.top() << endl;
        stack.pop();
    }
    cout << "此时栈是否为空?" << stack.empty() << endl;
}

void test1()
{
    Stack<string> stack;
    cout << "此时栈是否为空?" << stack.empty() << endl;
    stack.push(string(3, 'a'));
    cout << "此时栈是否为空?" << stack.empty() << endl;

    for(int i = 1; i < 12; ++i) {
        stack.push(string(3, 'a' + i));
    }
    cout << "此时栈是否已满?" << stack.full() << endl;
    while(!stack.empty()) {
        cout << stack.top() << endl;
        stack.pop();
    }
    cout << "此时栈是否为空?" << stack.empty() << endl;
}


int main()
{
    /* test0(); */
    test1();
    return 0;
}






