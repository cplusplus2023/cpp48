#include <48func.h>
int main()
{
    key_t key = 0x1234;
    int shmid = shmget(key,4096,IPC_CREAT|0600);
    ERROR_CHECK(shmid,-1,"shmget");
    char *p = (char *)shmat(shmid,NULL,0);
    ERROR_CHECK(p,(void *)-1,"shmat");
    printf("p = %s\n", p);
    shmdt(p);
    return 0;
}

