#include <48func.h>
int main(int argc, char *argv[])
{
    sigset_t set;
    sigfillset(&set);//所有信号都加入到set中
    sigset_t oldset;//保存之前的屏蔽状态
    sigprocmask(SIG_SETMASK,&set,&oldset);//屏蔽了所有的信号，原先的屏蔽状态存入oldset
    printf("Block all!\n");
    sleep(10);
    printf("Unblock all!\n");
    sigprocmask(SIG_SETMASK,&oldset,NULL);//恢复原先的屏蔽状态
    while(1);
    return 0;
}

