#include <iostream>
#include <sstream>

#include <alibabacloud/oss/OssClient.h>
using std::cout;
using std::endl;
using std::string;

using namespace AlibabaCloud::OSS;

struct OssInfo 
{
    string accessKeyId  = "LTAI5t6CJLdHYEP2vAmRa7PT";
    string accessKeySecret = "BXvRPQBYm8w7DGuJuzjRibKUpIR4eT";
    string endpoint = "oss-cn-hangzhou.aliyuncs.com";
    string bucketname = "bucket-lwh-test";
};

int main(void)
{
    OssInfo ossinfo;

    /* 初始化网络等资源。*/
    InitializeSdk();

    ClientConfiguration conf;
    OssClient client(ossinfo.endpoint, ossinfo.accessKeyId, ossinfo.accessKeySecret, conf);
    std::shared_ptr<std::iostream> content = std::make_shared<std::stringstream>();
    *content << "Thank you for using Alibaba Cloud Object Storage Service!!";
    string objectName ="tmp/2.txt";
    PutObjectRequest request(ossinfo.bucketname, objectName, content);

    /* 上传文件。*/
    auto outcome = client.PutObject(request);

    if (!outcome.isSuccess()) {
        /* 异常处理。*/
        std::cout << "PutObject fail" <<
        ",code:" << outcome.error().Code() <<
        ",message:" << outcome.error().Message() <<
        ",requestId:" << outcome.error().RequestId() << std::endl;
        return -1;
    }

    /* 释放网络等资源。*/
    ShutdownSdk();
    return 0;
}
