#include <memory> //vimplus快捷键  ,o
#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
using std::shared_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    ~Point() {  cout << "~Point()" << endl; }

    void print() const
    {   cout << "(" << _ix << "," << _iy << ")" << endl;}

private:
    int _ix;
    int _iy;
};  

shared_ptr<Point> getValue()
{
    shared_ptr<Point> sp(new Point(3, 4));
    return sp;
}

void test0()
{
    Point * pt = new Point(1, 2);
    shared_ptr<Point> sp(pt);
    sp->print();
    (*sp).print();
    cout << "sp.get(): " << sp.get() << endl;
    cout << "sp.use_count():" << sp.use_count() << endl;
    cout << "pt: " << pt << endl;

    cout << "\n进行复制操作:" << endl;
    //进行复制或者赋值操作, 引用计数加1
    shared_ptr<Point> sp2(sp);//ok
    cout << "sp.use_count():" << sp.use_count() << endl;
    cout << "sp2.use_count():" << sp2.use_count() << endl;
    
    shared_ptr<Point> sp3 = getValue();
    sp3->print();

    //shared_ptr可以放入容器中使用
    vector<shared_ptr<Point>> pointers;
    pointers.push_back(sp2);//引用计数加1
    cout << "sp.use_count():" << sp.use_count() << endl;
    cout << "sp2.use_count():" << sp2.use_count() << endl;
}


int main()
{
    test0();
    return 0;
}










