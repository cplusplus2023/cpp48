 ///
 /// @file    default.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 16:05:12
 ///
 
#include <iostream>
using std::cout;
using std::endl;

#if 0
int add(int x)
{	return x; }

int add(int x, int y)
{	return x + y;	}
#endif

//默认参数（缺省值）
//
//默认参数的设置必须遵循从右到左的顺序进行
//就是为了让程序员少写代码
//
//函数声明
int add(int x = 0, int y = 0, int z = 0);


void test0() 
{
	int a = 1, b = 2, c = 3;
	//add(, 1, 2);
	cout << "add(a):" << add(a) << endl;
	cout << "add(a, b):" << add(a, b) << endl;
	cout << "add(a, b, c):" << add(a, b, c) << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}

//当一个函数分成了声明与定义之后，
//默认值只需要在函数声明中给出，
//函数定义中不再需要重复给出
int add(int x, int y, int z)
{	return x + y + z;	}
