#include "TaskQueue.h"
#include "Producer.h"
#include "Consumer.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

void test()
{
    TaskQueue taskQue(10);

    Producer pro(taskQue);
    Consumer con(taskQue);

    pro.start();
    con.start();

    pro.join();
    con.join();

}
int main(int argc, char **argv)
{
    TaskQueue taskQue(10);
    unique_ptr<Thread> pro(new Producer(taskQue));
    unique_ptr<Thread> con(new Consumer(taskQue));

    pro->start();
    con->start();

    pro->join();
    con->join();

    /* MutexLock mutex; */
    /* MutexLock mutex2 = mutex;//error */
    return 0;
}

