#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#include <iostream>
using std::cout;
using std::endl;

int gNumber = 1000;
 
void * threadFunc1(void * arg)
{
	printf("thread1     thread_id: %lu, &errno: %p, &gNumber: %p, gNumber: %d\n\n", 
		   pthread_self(), &errno, &gNumber, gNumber);

	return nullptr;
}
 
void * threadFunc2(void * arg)
{
	printf("thread2     thread_id: %lu, &errno: %p, &gNumber: %p, gNumber: %d\n\n", 
		   pthread_self(), &errno, &gNumber, gNumber);

	return nullptr;
}

int main(void)
{
	printf("main_thread thread_id: %lu, &errno: %p, &gNumber: %p, gNumber: %d\n\n", 
			pthread_self(), &errno, &gNumber, gNumber);

	pthread_t thid1, thid2;
	pthread_create(&thid1, nullptr, threadFunc1, nullptr);
	pthread_create(&thid2, nullptr, threadFunc2, nullptr);

	pthread_join(thid1, nullptr);
	pthread_join(thid2, nullptr);

	return 0;
} 
