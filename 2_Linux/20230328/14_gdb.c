#include <48func.h>
int main(int argc, char *argv[])
{
    if(fork() == 0){
        printf("Child!\n");
    }
    else{
        printf("Parent!\n");
        wait(NULL);
    }
    return 0;
}

