#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
{
public:
    void process() 
    {
        cout << "thread_name : " << name << endl;

        while(1)
        {
            cout << "The thread is running" << endl;
            sleep(1);
        }
    }
};

int main(int argc, char **argv)
{
    MyTask task;
    unique_ptr<Thread> mythread(new Thread(bind(&MyTask::process, &task), "1号线程"));
    /* unique_ptr<Thread> mythread(new Thread(bind(&MyTask::process, &task))); */

    cout << "main thread : name = " << name << endl;

    mythread->start();
    mythread->join();

    return 0;
}
