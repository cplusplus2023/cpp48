#ifndef __TIMERFD_H__
#define __TIMERFD_H__

#include <functional>

using std::function;

class TimerFd
{
    using TimerFdCallback = function<void()>;
public:
    TimerFd(int initSec, int peridocSec, TimerFdCallback&&cb);
    ~TimerFd();

    void start();
    void stop();

    void handleRead();
    int createTimerFd();
    //设置定时器
    void setTimerFd(int initSec, int peridocSec);

private:
    int _timerfd;//timerfd_create返回的文件描述符
    int _initSec;//定时器的初始时间
    int _peridocSec;//定时器的周期时间
    bool _isStarted;//标志是否运行
    TimerFdCallback _cb;//需要执行的任务
    

};

#endif
