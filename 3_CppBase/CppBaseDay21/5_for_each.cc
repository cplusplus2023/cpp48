#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::copy;
using std::vector;
using std::ostream_iterator;

void func(int &value)
{
    ++value;
    cout << value << "  ";
}

void test()
{
    int a = 10;
    int b = 10;
    vector<int> number = {1, 3, 7, 9, 12, 67, 23};
    /* for_each(number.begin(), number.end(), func); */
    //C++11中的语法
    //lambda表达式===>匿名函数
    //[]捕获列表,捕获匿名函数外面的变量
# if 0
    for_each(number.begin(), number.end(), [&a, &b](int &value){
             cout << ++a << "  ";
             cout << b << "  ";
             ++value;
             cout << value << "  ";
             });
#endif
    for_each(number.begin(), number.end(), [](int &value){
             ++value;
             cout << value << "  ";
             });
    cout << endl;

    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

