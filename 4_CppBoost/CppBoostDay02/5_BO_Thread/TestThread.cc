#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
{
public:
    void process()
    {
        while(1)
        {
            cout << "MyTask is running!!!" << endl; 
            sleep(1);
        }
    }
};

void func(int x)
{
    while(1)
    {
        cout << "func is running!!!" << endl; 
        sleep(x);
    }
}

void test2()
{
    Thread th(bind(func, 1));

    th.start();
    th.join();
}

void test()
{
    MyTask task;

    Thread th(bind(&MyTask::process, &task));

    th.start();
    th.join();
}


int main(int argc, char **argv)
{
    test2();
    return 0;
}

