#include "ThreadPool.h"
#include <unistd.h>

ThreadPool::ThreadPool(size_t threadNum, size_t queSize)
: _threadNum(threadNum)
, _queSize(queSize)
, _taskQue(_queSize)
, _isExit(false)
{
    _threads.reserve(_threadNum);
}

ThreadPool::~ThreadPool()
{
    if(!_isExit)
    {
        stop();
        _isExit = true;
    }
}

//线程池开始执行的时候，其实就是工作线程已经开启
void ThreadPool::start()
{
    for(size_t idx = 0; idx < _threadNum; ++idx)
    {
        //这里有所改变==============================================
       unique_ptr<Thread> up(new Thread(std::bind(&ThreadPool::threadFunc, this), 
                                        std::to_string(idx)));
       _threads.push_back(std::move(up));
    }

    for(auto &th : _threads)
    {
        th->start();//创建工作线程的id，将工作线程开始运行
    }
}

//等同于在生产者消费者例子里面的生产者
void ThreadPool::addTask(Task &&task)
{
    if(task)
    {
        _taskQue.push(std::move(task));
    }
}

Task ThreadPool::getTask()
{
    return _taskQue.pop();
}

void ThreadPool::stop()
{
    //只要任务队列中有数据，线程池中的工作线程就不能退出，让其
    //sleep
    while(!_taskQue.empty())
    {
        sleep(1);
    }

    _isExit = true;
    //将所有等在在_notEmpty上的工作线程全部唤醒
    _taskQue.wakeup();
    for(auto &th : _threads)
    {
        th->join();
    }

}

//在线程池中封装的任务，这个任务的实际执行者WorkThread
void ThreadPool::threadFunc()
{
    while(!_isExit)
    {
        Task taskcb = getTask();
        if(taskcb)
        {
            taskcb();
        }
    }
}
