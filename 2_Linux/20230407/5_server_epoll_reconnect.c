#include <48func.h>
int main(int argc, char *argv[])
{
    // ./5_server_epoll_reconnect 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);//ipv4 tcp
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(atoi(argv[2]));
    serverAddr.sin_addr.s_addr = inet_addr(argv[1]);
    int reuseArg = 1;
    int ret = setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&reuseArg,sizeof(reuseArg));
    ERROR_CHECK(ret,-1,"setsockopt");
    ret = bind(sockfd,(struct sockaddr *)&serverAddr,sizeof(serverAddr));
    ERROR_CHECK(ret,-1,"bind");
    ret = listen(sockfd,10);
    ERROR_CHECK(ret,-1,"listen");
    char buf[4096] = {0};
    int epfd = epoll_create(1);
    struct epoll_event event;
    event.events = EPOLLIN;// sockfd accept 是一种读行为
    event.data.fd = sockfd;
    int netfd = -1; // netfd为-1时，说明无客户端连接
    time_t lastactive;
    epoll_ctl(epfd,EPOLL_CTL_ADD,sockfd,&event);
    while(1){
        struct epoll_event readyset[3];
        int readynum = epoll_wait(epfd,readyset,3,1000);// 1000ms --> 1s
        time_t curtime = time(NULL);
        printf("curtime = %s\n", ctime(&curtime));
        for(int i = 0; i < readynum; ++i){
            if(readyset[i].data.fd == sockfd){
                printf("Haha!\n");
                netfd = accept(sockfd,NULL,NULL);
                lastactive = time(NULL);
                // 把sockfd移除监听
                epoll_ctl(epfd,EPOLL_CTL_DEL,sockfd,NULL);
                // 把netfd和stdin加入监听
                event.events = EPOLLIN;
                event.data.fd = netfd;
                epoll_ctl(epfd,EPOLL_CTL_ADD,netfd,&event);
                event.events = EPOLLIN;
                event.data.fd = STDIN_FILENO;
                epoll_ctl(epfd,EPOLL_CTL_ADD,STDIN_FILENO,&event);
            }
            else if(readyset[i].data.fd == STDIN_FILENO){
                bzero(buf,sizeof(buf));
                ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
                send(netfd,buf,strlen(buf),0);
            }
            else if(readyset[i].data.fd == netfd){
                lastactive = time(NULL);
                bzero(buf,sizeof(buf));
                ssize_t sret = recv(netfd,buf,sizeof(buf),0);
                if(sret == 0){ // 对面close了
                    epoll_ctl(epfd,EPOLL_CTL_DEL,netfd,NULL);
                    epoll_ctl(epfd,EPOLL_CTL_DEL,STDIN_FILENO,NULL);
                    event.events = EPOLLIN;
                    event.data.fd = sockfd;
                    epoll_ctl(epfd,EPOLL_CTL_ADD,sockfd,&event);
                    close(netfd);
                    netfd = -1;
                }
                printf("buf = %s\n", buf);
            }
        }

        if(netfd != -1 && curtime - lastactive > 5){
            printf("Hehe!\n");
            epoll_ctl(epfd,EPOLL_CTL_DEL,netfd,NULL);
            epoll_ctl(epfd,EPOLL_CTL_DEL,STDIN_FILENO,NULL);
            event.events = EPOLLIN;
            event.data.fd = sockfd;
            epoll_ctl(epfd,EPOLL_CTL_ADD,sockfd,&event);
            close(netfd);
            netfd = -1;
        }
    }
    return 0;
}






