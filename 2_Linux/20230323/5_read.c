#include <48func.h>
int main(int argc, char *argv[])
{
    // ./5_read file2
    ARGS_CHECK(argc,2);
    int fd = open(argv[1], O_RDWR|O_CREAT, 0666);
    ERROR_CHECK(fd,-1,"open");
    int val;
    ssize_t sret = read(fd,&val,sizeof(val));
    printf("sret = %ld, val = %d\n", sret, val);
    return 0;
}

