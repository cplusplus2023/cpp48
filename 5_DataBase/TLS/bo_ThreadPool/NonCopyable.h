#ifndef  __NOCOPTABLE_H__
#define __NOCOPTABLE_H__

class NonCopyable
{
public:
protected:
    NonCopyable()
    {

    }
    ~NonCopyable()
    {

    }

    NonCopyable(const NonCopyable &rhs) = delete;
    NonCopyable &operator=(const NonCopyable &rhs) = delete;
};

#endif
