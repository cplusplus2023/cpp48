#include <48func.h>
int main()
{
    int pipefd[2];
    pipe(pipefd);
    if(fork() == 0){
        // pipefd 子进程 --> 父进程
        close(pipefd[0]);// 关闭子进程的读端
        sleep(10);
        write(pipefd[1],"niganma",7);
    }
    else{
        close(pipefd[1]);// 关闭父进程的写端
        char buf[1024] = {0};
        read(pipefd[0],buf,sizeof(buf));
        printf("I am parent, buf = %s\n", buf);
        wait(NULL);
    }
    return 0;
}

