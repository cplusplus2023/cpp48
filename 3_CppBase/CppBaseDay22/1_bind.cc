#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

class Example
{
public:
    //Example *this
    int add(int x, int y)
    {
        cout << "int Example::add(int, int)" << endl;
        return x + y;
    }
};

void test()
{
    Example ex;
    auto f = bind(&Example::add, &ex, 10, 20);
    cout << "f() = " << f() << endl;

    cout << endl;
    auto f2 = bind(&Example::add, ex, 100, 200);
    cout << "f2() = " << f2() << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

