#include <signal.h>
#include <iostream>

#include <workflow/WFFacilities.h>
#include <workflow/WFHttpServer.h>

using std::cout;
using std::endl;

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    cout << "sighandler is processing" << endl;
    waitGroup.done();
}

void httpCallback(WFHttpTask * serverTask)
{
    cout << "server httpCallback is running" << endl;
}

void process(WFHttpTask * serverTask)
{
    cout << "process funciton is running\n";
    //设置任务的回调函数
    serverTask->set_callback(httpCallback);
}

void test0()
{
    signal(SIGINT, sighandler);
    
    WFHttpServer server(process);

    if(server.start(8888) == 0) {
        waitGroup.wait();
        server.stop();
    } else {
        cout << "server cannot start" << endl;
    }
}


int main()
{
    test0();
    return 0;
}

