#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <alibabacloud/oss/OssClient.h>
using namespace AlibabaCloud::OSS;

struct OssInfo 
{
    string accessKeyId  = "LTAI5t6CJLdHYEP2vAmRa7PT";
    string accessKeySecret = "BXvRPQBYm8w7DGuJuzjRibKUpIR4eT";
    string endpoint = "oss-cn-hangzhou.aliyuncs.com";
    string bucketname = "bucket-lwh-test";
};

int main(void)
{
    OssInfo ossinfo;

    /* 初始化网络等资源。*/
    InitializeSdk();

    ClientConfiguration conf;
    OssClient client(ossinfo.endpoint, ossinfo.accessKeyId, ossinfo.accessKeySecret, conf);

    /* 设置签名URL有效时长。*/
    time_t t = time(nullptr) + 1200;
    /* 生成签名URL。*/
    string getObjectName = "oss/1.txt";
    auto genOutcome = client.GeneratePresignedUrl(ossinfo.bucketname, getObjectName, t, Http::Get);
    if (genOutcome.isSuccess()) {
        std::cout << "GeneratePresignedUrl success, Gen url:" << genOutcome.result().c_str() << std::endl;
    }
    else {
        /* 异常处理。*/
        std::cout << "GeneratePresignedUrl fail" <<
        ",code:" << genOutcome.error().Code() <<
        ",message:" << genOutcome.error().Message() <<
        ",requestId:" << genOutcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return -1;
    }

#if 0
    /* 使用签名URL下载文件。*/
    auto outcome = client.GetObjectByUrl(genOutcome.result());

    if (!outcome.isSuccess()) {
        /* 异常处理。*/
        std::cout << "GetObjectByUrl fail" <<
        ",code:" << outcome.error().Code() <<
        ",message:" << outcome.error().Message() <<
        ",requestId:" << outcome.error().RequestId() << std::endl;
        ShutdownSdk();
        return -1;
    }
#endif

    /* 释放网络等资源。*/
    ShutdownSdk();
    return 0;
}
