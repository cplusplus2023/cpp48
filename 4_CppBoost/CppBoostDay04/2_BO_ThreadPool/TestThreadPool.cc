#include "ThreadPool.h"
#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

class MyTask
{
public:
    void process() 
    {
        ::srand(::clock());
        int number = ::rand()%100;
        cout << ">>BO_ThreadPool. MyTask number = " << number << endl;
    }
};


int main(int argc, char **argv)
{
    unique_ptr<MyTask> ptask(new MyTask());
    ThreadPool pool(4, 10);

    pool.start();

    int cnt = 20;
    while(cnt-- > 0)
    {
        pool.addTask(std::bind(&MyTask::process, ptask.get()));
        cout << "cnt = " << cnt << endl;
    }

    pool.stop();
    
    return 0;
}

