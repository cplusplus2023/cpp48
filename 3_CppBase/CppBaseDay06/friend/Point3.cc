 ///
 /// @file    Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-25 16:30:00
 ///
 
#include <math.h>
#include <iostream>
using std::cout;
using std::endl;

class Point;//类的前向声明
class Line
{
public:
	float getDistance(const Point & lhs, const Point & rhs);
	void setPoint(Point & pt, int x, int y);

private:
	int _iz;
};

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Point(int=0,int=0)" << endl;}

	~Point() {	cout << "~Point()" << endl;	}

	void print() const
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")";
	}

	//Point并不是Line的友元
	void setZ(Line & line, int z)
	{
		line._iz = z;
	}

	//友元之友元类
	friend class Line;

private:
	int _ix;
	int _iy;
};

float Line::getDistance(const Point & lhs, const Point & rhs)
{
	return sqrt((lhs._ix - rhs._ix) * (lhs._ix - rhs._ix) +
				(lhs._iy - rhs._iy) * (lhs._iy - rhs._iy));
}

void Line::setPoint(Point & pt, int x, int y)
{
	pt._ix = x;
	pt._iy = y;
}
 
void test0() 
{
	Point p1(1, 2), p2(3, 4);
	p1.print();
	cout << " --->";
	p2.print();
	/* Line line; */
	//Line();//匿名对象
	cout << "的距离是:" << Line().getDistance(p1, p2) << endl;

	Line().setPoint(p1, 11, 12);
	cout << "p1:";
	p1.print();
	cout << endl;

} 
 
int main(void)
{
	test0();
	return 0;
}
