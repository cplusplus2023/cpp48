#include <48func.h>
void handler(int signum){
    printf("signum = %d\n", signum);
}
int main(int argc, char *argv[])
{
    //signal(SIGINT,handler);
    struct sigaction act;
    memset(&act,0,sizeof(act));
    act.sa_handler = handler;
    act.sa_flags = SA_RESTART|SA_RESETHAND;
    sigaction(SIGINT,&act,NULL);
    char buf[1024] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

