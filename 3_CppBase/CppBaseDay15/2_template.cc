#include <iostream>
using std::cout;
using std::endl;

//
//          实例化
//由函数模板  ==》 模板函数
template <class Type>
Type add(Type x, Type y)
{
    return x + y;
}

#if 0
int add(int x, int y)
{   return x + y;   }

long add(long x, long y)
{   return x + y;   }

float add(float x, float y)
{   return x + y;   }
#endif

void test0()
{
    int a1 = 1, a2 = 2;
    long b1 = 10, b2 = 20;
    float c1 = 1.1, c2 = 2.2;
    //add(a1, a2)  ==> 隐式实例化
    cout << "add(a1, a2): " << add(a1, a2) << endl;
    //在模板实参列表中指定参数类型, 显式实例化
    //add<long>(b1, b2)
    //cout << "add(b1, b2): " << add<long>(b1, b2) << endl;
    cout << "add(c1, c2): " << add(c1, c2) << endl;
    //隐式实例化时推导失败
    //cout << "add(a1, b1): " << add(a1, b1) << endl;
    cout << "add(a1, b1): " << add<long>(a1, b1) << endl;
}


int main()
{
    test0();
    return 0;
}

