
#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <SimpleAmqpClient/SimpleAmqpClient.h>

struct AmqpInfo
{
    string amqpurl = "amqp://guest:guest@localhost:5672";
    string exchanger = "uploadserver.trans";
    string amqpqueue = "uploadserver.trans.oss";
    string routingkey = "oss";
};

void test_publish()
{
    AmqpInfo amqpinfo; 
    AmqpClient::Channel::ptr_t channel = AmqpClient::Channel::Create();
    AmqpClient::BasicMessage::ptr_t message = AmqpClient::BasicMessage::Create("hello,rabbitmq");
    channel->BasicPublish(amqpinfo.exchanger, amqpinfo.routingkey, message);
    pause();
}

void test_consumer()
{
    AmqpInfo amqpinfo; 
    AmqpClient::Channel::ptr_t channel = AmqpClient::Channel::Create();
    channel->BasicConsume(amqpinfo.amqpqueue);

    AmqpClient::Envelope::ptr_t envelop;
    bool flag = channel->BasicConsumeMessage(envelop, 3000);
    if(flag == false) {
        cout << "get message timeout" << endl;
        return;
    }

    string msg = envelop->Message()->Body();
    cout << "message:" << msg << endl;
}


int main()
{
    /* test_publish(); */
    test_consumer();
    return 0;
}

