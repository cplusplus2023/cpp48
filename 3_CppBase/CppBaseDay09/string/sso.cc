 ///
 /// @file    sso.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 17:36:09
 ///
 
#include <iostream>
using std::cout;
using std::endl;
using std::string;
 
void test0() 
{
	//s1和s2都在栈上, 拷贝的速度非常快
	string s1("hello");
	cout << "sizeof(s1):" << sizeof(s1) << endl;
	string s2("helloworldhello");
	cout << "sizeof(s1):" << sizeof(s2) << endl;
	printf("s2'c_str: %p\n", s2.c_str());
	cout << "&s2:" << &s2 << endl;

	string s3("helloworldhelloa");
	cout << "sizeof(s3):" << sizeof(s3) << endl;
	printf("s3'c_str: %p\n", s3.c_str());
	cout << "&s3:" << &s3 << endl;

	string s4 = s3;//执行的是深拷贝
	printf("s4'c_str: %p\n", s4.c_str());
 
} 
 
int main(void)
{
	test0();
	return 0;
}
