 ///
 /// @file    Line.hpp
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 11:44:28
 ///
 
#ifndef __LINE_HPP__
#define __LINE_HPP__

#include <iostream>
using std::cout;
using std::endl;
 
class Line
{
	//将Line分成了头文件和实现文件以后，还是能看到
	//Line的内部实现的
	class Point
	{
	public:
		Point(int ix = 0, int iy = 0);

		friend std::ostream & operator<<(std::ostream & os, 
				const Point & rhs);

		~Point() {	cout << "~Point()" << endl;	}

	private:
		int _ix;
		int _iy;
	};
 
public:
	Line(int x1, int y1, int x2, int y2)
	: _pt1(x1, y1)
	, _pt2(x2, y2)
	{	cout << "Line(int,int,int,int)" << endl;	}

	void printLine() const;
	
	friend std::ostream & operator<<(std::ostream & os, const Point & rhs);

private:
	Point _pt1;
	Point _pt2;
};
 
#endif
