 ///
 /// @file    9_constPoint.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 17:23:38
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{
		cout << "Point(int,int)" << endl;
	}

#if 1
	void print(/* Point * const this */)
	{
		_ix = 100;//ok
		cout << " print()" << endl;
		cout << "(" << _ix
			 << "," << _iy 
			 << ")" << endl;
	}
#endif


	//const成员函数的const关键字影响的是this的形式
	//
	//注意：只要成员函数没有对数据成员进行修改，就必须要将其设计成const的
	void print(/* const Point * const this */) const
	{
		//_ix =100;//error
		cout << "print() const" << endl;
		cout << "(" << _ix
			 << "," << _iy 
			 << ")" << endl;
	}
private:
	int _ix;
	int _iy;
};
 
void test0() 
{
	Point p(1, 2);
	p.print();//当只有const成员函数时，非const对象可以调用const成员函数

	//const对象只能调用const成员函数
	const Point p2(11, 12);
	p2.print();
	
} 
 
int main(void)
{
	test0();
	return 0;
}
