 ///
 /// @file    atexit.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 15:16:23
 ///
 
#include <iostream>
using std::cout;
using std::endl;

void display()
{	cout << "display()" << endl;	}

void print()
{	cout << "print()" << endl;}
 
void test0() 
{
	cout << "enter test0()" << endl;
	//被注册的函数会在程序退出时，自动调用；
	//调用的顺序正好是遵循栈结构
	atexit(display);
	atexit(display);
	atexit(print);
} 
 
int main(void)
{
	test0();
	cout << "exit main()" << endl;
	return 0;
}
