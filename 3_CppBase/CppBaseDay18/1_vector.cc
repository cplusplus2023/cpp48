#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test()
{
    //初始化
    /* vector<int>  number();//注意：函数的声明 */
    /* vector<int>  number;//1、创建一个空对象 */
    /* vector<int>  number(10);//2、count个value值 */
    /* int arr[10] = {1, 3, 5, 7, 9, 8, 6, 4, 2, 0}; */
    //3、迭代器范围的形式
    /* vector<int> number(arr, arr + 10);//左闭右开[arr, arr + 10) */
    //4、拷贝构造函数（或者移动构造函数）
    /* vector<int>  number2(number); */
    //5、大括号的形式
    vector<int> number = {1, 2, 3, 5, 7, 9, 8, 6, 4};

    //遍历
    //1、取下标
    for(size_t idx = 0; idx != number.size(); ++idx)
    {
        cout << number[idx] << "  ";
    }
    cout << endl;

    //2、未初始化迭代器
    //it就是迭代器，也就是一个广义的指针
    vector<int>::iterator it;
    for(it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

    //3、初始化迭代器
    vector<int>::iterator it2 = number.begin();
    for(; it2 != number.end(); ++it2)
    {
        cout << *it2 << "  ";
    }
    cout << endl;

#if 0
    for(size_t idx = 0; idx != number2.size(); ++idx)
    {
        cout << number2[idx] << "  ";
    }
    cout << endl;
#endif
    //4、for与auto
    for(auto &elem : number)
    {
        cout << elem << "  ";
    }
    cout << endl;

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

