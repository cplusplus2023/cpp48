#include <iostream>
using std::cout;
using std::endl;
using std::string;


class SafeFile
{
public: 
    //在构造函数中托管FILE文件指针
    SafeFile(FILE * fp)
    : _fp(fp)
    {}

    //提供访问文件指针的操作
    void write(const string & msg)
    {
        fwrite(msg.c_str(), 1, msg.size(), _fp);
    }

    SafeFile(const SafeFile&) = delete;
    SafeFile & operator=(const SafeFile &) = delete;


    //在析构函数中释放文件指针
    ~SafeFile() {
        if(_fp) {
            fclose(_fp);
            cout << " >> fclose(fp)" << endl;
        }
    }

private:
    FILE * _fp;
};


void test0()
{
    SafeFile sf(fopen("test.txt", "a+"));
    string msg("this is a test\n");
    sf.write(msg);

}


int main()
{
    test0();
    return 0;
}

