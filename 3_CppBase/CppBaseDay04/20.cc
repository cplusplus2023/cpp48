 ///
 /// @file    20.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 11:48:19
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
	A() {	cout << "A()" << endl;	}
	~A() {	cout << "~A()" << endl;	}
};
 
class B
{
public:
	B() {	cout << "B()" << endl;	}
	~B() {	cout << "~B()" << endl;	}

	void print() {}
};

class C
{
public:
	C() {	cout << "C()" << endl;	}
	~C() {	cout << "~C()" << endl;	}
};
class D
{
public:
	D() {	cout << "D()" << endl;	}
	~D() {	cout << "~D()" << endl;	}
};
//全局静态区的对象销毁顺序是栈结构
//先创建的，后销毁
C c;
void test0() 
{
	A *pa=new A();
    B b();//函数声明 
	//b.print();
    static D d;
    delete pa; 
} 
 
int main(void)
{
	test0();
	return 0;
}
