#ifndef __THREAD_H__//防止该文件被同一个文件包含多次之后，多次编译的问题
#define __THREAD_H__

#include <pthread.h>
#include <functional>
#include <string>

using std::function;
using std::bind;
using std::string;

extern __thread const char *name;
//string不是一个POD类型，所以不能写成__thread变量
/* extern __thread string name1; */
/* __thread int idx = 0; */

using ThreadCallback = function<void()>;

class Thread
{
public:
    Thread(ThreadCallback &&cb, const string &name = string());
    /* Thread(ThreadCallback &&cb); */
    ~Thread();
    void start();
    void join();

private:
    //线程入口函数
    static void *threadFunc(void *arg);

private:
    pthread_t _thid;
    string _name;
    bool _isRunning;
    ThreadCallback _cb;
};

#endif
