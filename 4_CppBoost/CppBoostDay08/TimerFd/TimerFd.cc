#include "TimerFd.h"
#include <unistd.h>
#include <poll.h>
#include <sys/timerfd.h>
#include <iostream>

using std::cerr;
using std::cout;
using std::endl;

TimerFd::TimerFd(int initSec, int peridocSec, TimerFdCallback &&cb)
: _timerfd(createTimerFd())
, _initSec(initSec)
, _peridocSec(peridocSec)
, _isStarted(false)
, _cb(std::move(cb))
{

}

TimerFd::~TimerFd()
{
    setTimerFd(0, 0);
    close(_timerfd);
}

void TimerFd::start()
{
    struct pollfd pfd;
    pfd.fd = _timerfd;
    pfd.events = POLLIN;

    setTimerFd(_initSec, _peridocSec);

    _isStarted = true;
    while(_isStarted)
    {
        int nready = poll(&pfd, 1, 5000);
        cout << "nready = " << nready << endl;
        if(-1 == nready && errno == EINTR)
        {
            continue;
        }
        else if(-1 == nready)
        {
            cerr << "-1 == nready " << endl;
            return;
        }
        else if(0 == nready)
        {
            cout << ">>poll_timeout!!!" << endl;
        }
        else
        {
            if(pfd.revents & POLLIN)
            {
                handleRead();//如果执行该函数的线程被唤醒，就可以执行任务
                if(_cb)
                {
                    _cb();//执行了任务
                }
            }
        }
    }
}

void TimerFd::stop()
{
    _isStarted = false;
    setTimerFd(0, 0);
}

void TimerFd::handleRead()
{
    uint64_t one = 1;
    int ret = read(_timerfd, &one, sizeof(one));
    if(ret != sizeof(one))
    {
        perror("read");
        return;
    }
}

int TimerFd::createTimerFd()
{
    int fd = timerfd_create(CLOCK_REALTIME, 0);
    if(fd < 0)
    {
        perror("timerfd_create");
        return fd;
    }
    return fd;
}

void TimerFd::setTimerFd(int initSec, int peridocSec)
{
    struct itimerspec value;

    value.it_value.tv_sec = initSec;
    value.it_value.tv_nsec = 0;

    value.it_interval.tv_sec = peridocSec;
    value.it_interval.tv_nsec = 0;

    int fd = timerfd_settime(_timerfd, 0, &value, nullptr);
    if(fd < 0)
    {
        perror("timerfd_settime");
    }
}
