#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;
using std::function;

//普通函数(非成员函数)
int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}

int func(int x, int y, int z)
{
    cout << "int func(int, int, int)" << endl;
    return x * y * z;
}

class Example
{
public:
    int add(int x, int y)
    {
        cout << "int Example::add(int, int)" << endl;
        return x + y;
    }

    //数据成员
    int data = 100;//C++11
};

void test()
{
    /* vector<int> number = {1, 2, 3}; */
    //函数的形态、类型、标签
    //函数类型：函数的返回类型 + 函数的参数列表
    //add函数的类型：int(int, int)
    //f的函数类型：int()
    //bind可以改变函数的形态（重要）：
    //可以改变函数参数的个数(减少函数的参数个数)
    //function可以存函数类型，所以可以将function称为函数的容器
    function<int()> f = bind(&add, 1, 2);
    cout << "f() = " << f() << endl;

    //func函数的类型：int(int, int, int)
    //f2的函数类型：int()
    function<int()> f2 = bind(&func, 1, 3, 5);
    cout << "f2() = " << f2() << endl;

    Example ex;
    //add函数的形态：int(this, int, int)
    //f3的函数类型：int()
    function<int()> f3 = bind(&Example::add, &ex, 10, 20);
    cout << "f3() = " << f3() << endl;

    using namespace std::placeholders;//占位符
    //add函数形态：int(int, int)
    //f4的函数类型：int(int)
    function<int(int)> f4 = bind(add, 1, _1);
    cout << "f4(9) = " << f4(9) << endl;

    //bind不仅可以绑定到普通函数，也可以绑定到成员函数，甚至
    //还可以绑定到数据成员
    Example ex2;
    f3 = bind(&Example::data, &ex2);
    cout << "f3() = " << f3() << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

