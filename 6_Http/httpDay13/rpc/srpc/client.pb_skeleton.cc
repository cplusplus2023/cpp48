
#include "user.srpc.h"
#include "workflow/WFFacilities.h"

using namespace srpc;

static WFFacilities::WaitGroup wait_group(1);

void sig_handler(int signo)
{
	wait_group.done();
}

static void sigup_done(RespSignup *response, srpc::RPCContext *context)
{
    int code = response->code();
    std::string message = response->message();
    std::cout << "code: " << code << ", message:" << message << std::endl;
}

int main()
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	const char *ip = "127.0.0.1";
	unsigned short port = 1412;

	UserService::SRPCClient client(ip, port);

	// example for RPC method call
	ReqSignup sigup_req;
    sigup_req.set_username("Jackie");
    sigup_req.set_password("123");
	client.Sigup(&sigup_req, sigup_done);

	wait_group.wait();
	google::protobuf::ShutdownProtobufLibrary();
	return 0;
}
