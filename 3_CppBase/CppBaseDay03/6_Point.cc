 ///
 /// @file    3_Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 10:21:04
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{
		cout << "Point(int =0,int=0)" << endl;
	}

	void print() 
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")" << endl;
	}

	//当没有显式提供析构函数时，系统也会自动提供一个析构函数
	~Point()
	{
		cout << "~Point()" << endl;
	}

private:
	int _ix;
	int _iy;
};

Point gpt(11, 12);
static Point gpt2(12, 12);

void test1()
{
	Point pt(10);
	cout << "pt:";
	pt.print();
	//析构函数可以主动调用
	//
	//但一般情况下，析构函数会自动调用
	//pt.~Point();
	
	Point * p = new Point;
	p->print();

	delete p;
}

 
void test0() 
{
	int a = 1;//初始化
	a = 2;//赋值语句

	int b(3);
	cout << "b: " << b << endl;

	Point pt;
	pt.print();

	Point pt2(1, 2);
	cout << "pt2:";
	pt2.print();

} 
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
