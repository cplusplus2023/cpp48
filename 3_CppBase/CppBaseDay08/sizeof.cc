 ///
 /// @file    sizeof.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 09:56:42
 ///
 
#include <iostream>
using std::cout;
using std::endl;
 
void test0() 
{
	int number = 1;
	cout << sizeof(number) << endl;
	//sizeof并不是函数，而是一个长度运算符
	//起作用的时机是在编译时,而不是运行时
	cout << (sizeof number) << endl;
	printf("sizeof(number): %d\n", sizeof number);
} 
 
int main(void)
{
	test0();
	return 0;
}
