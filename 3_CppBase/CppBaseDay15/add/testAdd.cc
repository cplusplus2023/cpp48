#include "add.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    int a1 = 1, a2 = 2;
    cout << "add(a1, a2):" << add(a1, a2) << endl;
}


int main()
{
    test0();
    return 0;
}

