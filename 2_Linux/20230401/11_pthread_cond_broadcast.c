#include <48func.h>
typedef struct shareRes_s {
    int num;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} shareRes_t;
void *threadFunc1(void *arg){
    shareRes_t * pshareRes = (shareRes_t *)arg; 
    pthread_mutex_lock(&pshareRes->mutex);
    while(pshareRes->num == 0){
        pthread_cond_wait(&pshareRes->cond,&pshareRes->mutex);
    }
    printf("Before child thread1 wake up! num = %d\n", pshareRes->num);
    --pshareRes->num;
    printf("After child thread1 wake up! num = %d\n", pshareRes->num);
    pthread_mutex_unlock(&pshareRes->mutex);
    pthread_exit(NULL);
}
void *threadFunc2(void *arg){
    shareRes_t * pshareRes = (shareRes_t *)arg; 
    pthread_mutex_lock(&pshareRes->mutex);
    while(pshareRes->num == 0){
        pthread_cond_wait(&pshareRes->cond,&pshareRes->mutex);
    }
    printf("Before child thread2 wake up! num = %d\n", pshareRes->num);
    --pshareRes->num;
    printf("After child thread2 wake up! num = %d\n", pshareRes->num);
    pthread_mutex_unlock(&pshareRes->mutex);
    pthread_exit(NULL);

}
int main()
{
    shareRes_t shareRes;
    shareRes.num = 0;
    pthread_mutex_init(&shareRes.mutex, NULL);
    pthread_cond_init(&shareRes.cond, NULL);
    pthread_t tid1,tid2;
    pthread_create(&tid1,NULL,threadFunc1,&shareRes);
    pthread_create(&tid2,NULL,threadFunc2,&shareRes);
    sleep(1);
    pthread_mutex_lock(&shareRes.mutex);
    ++shareRes.num;
    //pthread_cond_signal(&shareRes.cond);
    pthread_cond_broadcast(&shareRes.cond);
    pthread_mutex_unlock(&shareRes.mutex);
    pthread_join(tid1,NULL);
    pthread_join(tid2,NULL);
    return 0;
}

