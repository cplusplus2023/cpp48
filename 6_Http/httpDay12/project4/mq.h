#ifndef __WD_mq_HPP__ 
#define __WD_mq_HPP__ 

#include <string>

#include <SimpleAmqpClient/SimpleAmqpClient.h>

using std::string;

struct AmqpInfo
{
    string amqpurl = "amqp://guest:guest@localhost:5672";
    string exchanger = "uploadserver.trans";
    string amqpqueue = "uploadserver.trans.oss";
    string routingkey = "oss";
};

class MessagePublisher
{
public:
    MessagePublisher(const AmqpInfo & amqpinfo, const string & msg)
    : _amqpinfo(amqpinfo)
    , _msg(msg)
    , _channel(AmqpClient::Channel::Create())
    {}

    void doPublish() {
        AmqpClient::BasicMessage::ptr_t message =
            AmqpClient::BasicMessage::Create(_msg);
        _channel->BasicPublish(_amqpinfo.exchanger, _amqpinfo.routingkey, message);
    }

private:
    AmqpInfo _amqpinfo;
    string _msg;
    AmqpClient::Channel::ptr_t _channel;
};

class MessageConsumer
{
public:
    MessageConsumer(const AmqpInfo & info)
    : _info(info)
    , _channel(AmqpClient::Channel::Create())
    {
        //启动一个消费者队列
        _channel->BasicConsume(_info.amqpqueue);
    }

    bool doConsume(string & msg)
    {
        AmqpClient::Envelope::ptr_t envelope;
        bool flag = _channel->BasicConsumeMessage(envelope, 3000);
        if(flag == false) {
            printf("get message timeout\n");           
            return false;
        } else {
            msg = envelope->Message()->Body();
            return true;
        }
    }

private:
    AmqpInfo _info;
    AmqpClient::Channel::ptr_t _channel;
};



#endif

