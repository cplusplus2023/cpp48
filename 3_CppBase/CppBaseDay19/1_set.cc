#include <iostream>
#include <set>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::set;
using std::vector;
using std::pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //set的特征
    //1、key值是唯一的，不能重复
    //2、默认情况下，会按照key值升序排列
    //3、set的底层使用的是红黑树
    /* set<int, std::greater<int>> number = {1, 3, 7, 9, 8, 4, 3, 7}; */
    set<int> number = {1, 3, 7, 9, 8, 4, 3, 7};
    display(number);

    cout << endl << "set中元素的查找操作" << endl;
    size_t cnt1 = number.count(3);
    size_t cnt2 = number.count(10);
    cout << "cnt1 = " << cnt1 << endl;
    cout << "cnt2 = " << cnt2 << endl;

    set<int>::iterator it = number.find(12);
    if(it == number.end())
    {
        cout << "该元素不存在set中" << endl;
    }
    else
    {
        cout << "该元素存在 = " << *it << endl;
    }

    cout << endl << "set的insert操作" << endl;
    /* pair<set<int>::iterator, bool> ret = number.insert(5); */
    auto ret = number.insert(5);
    if(ret.second)
    {
        cout << "插入成功 " << *ret.first << endl;
    }
    else
    {
        cout << "插入失败，该元素存在set中" << endl;
    }
    display(number);

    cout << endl;
    vector<int> vec = {11, 33, 99, 2, 7, 19};
    number.insert(vec.begin(), vec.end());//左闭右开
    display(number);

    cout << endl;
    /* number.insert({6, 77, 22}); */
    number.insert(std::initializer_list<int>{6, 77, 22});
    display(number);

    cout << endl << "set的下标操作" << endl;
    /* cout << "number[1] = " << number[1] << endl; */

    cout << endl << "set的修改操作" << endl;
    it = number.begin();
    /* *it = 100;//error */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

