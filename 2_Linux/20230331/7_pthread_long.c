#include <48func.h>
void *threadFunc(void *arg){
    long val = (long) arg;
    printf("child, val = %ld\n", val);
    ++val;
}
int main()
{
    long val = 1000;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,(void *)val);
    sleep(1);
    printf("main, val = %ld\n", val);
    return 0;
}

