#include "threadPool.h"
typedef struct train_s {
    int length;
    char data[1000];
} train_t;
//int transFile(int netfd){
//    train_t train = {5, "file1"};
//    send(netfd,&train,sizeof(int)+train.length,0);// 发文件的名字
//    int fd = open("file1", O_RDWR);
//    ssize_t sret = read(fd,train.data,sizeof(train.data)); 
//    train.length = sret;
//    send(netfd,&train,sizeof(int)+train.length,0);// 发文件的内容
//    return 0;
//}
//int transFile(int netfd){
//    train_t train = {5, "file1"};
//    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的名字
//    int fd = open("file1", O_RDWR);
//    struct stat statbuf;
//    fstat(fd,&statbuf);
//    train.length = sizeof(statbuf.st_size);
//    memcpy(train.data,&statbuf.st_size,train.length);
//    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的大小
//    while(1){
//        bzero(&train,sizeof(train));
//        ssize_t sret = read(fd,train.data,sizeof(train.data)); 
//        train.length = sret;
//        //即使文件已经读完了，也发一个空车厢火车过去
//        //目标是让客户端知道文件已经发完了
//        send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的内容
//        if(sret == 0){
//            break;
//        }
//    }
//    return 0;
//}
int transFile(int netfd){
    train_t train = {5, "file1"};
    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的名字
    int fd = open("file1", O_RDWR);
    struct stat statbuf;
    fstat(fd,&statbuf);
    train.length = sizeof(statbuf.st_size);
    memcpy(train.data,&statbuf.st_size,train.length);
    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的大小
    char *p = (char *)mmap(NULL,statbuf.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    ERROR_CHECK(p,MAP_FAILED,"mmap");
    off_t totalsize = 0;//记录已经发送的长度
    while(totalsize < statbuf.st_size){
        if(statbuf.st_size - totalsize > 1000){
            train.length = 1000;//剩余未发送的内容超过1000
        }
        else{
            train.length = statbuf.st_size - totalsize;
            //剩余内容不足1000
        }
        send(netfd,&train.length,sizeof(train.length),MSG_NOSIGNAL);//发火车头
        send(netfd,p+totalsize,train.length,MSG_NOSIGNAL);//火车车厢在mmap映射区
        totalsize += train.length;
    }
    // 补发一个空火车
    train.length = 0;
    send(netfd,&train.length,sizeof(train.length),MSG_NOSIGNAL);//发火车头
    munmap(p,statbuf.st_size);
    close(fd);
    return 0;
}
//int transFile(int netfd){
//    train_t train = {5, "file1"};
//    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的名字
//    int fd = open("file1", O_RDWR);
//    struct stat statbuf;
//    fstat(fd,&statbuf);
//    train.length = sizeof(statbuf.st_size);
//    memcpy(train.data,&statbuf.st_size,train.length);
//    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的大小
//   // char *p = (char *)mmap(NULL,statbuf.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
//   // ERROR_CHECK(p,MAP_FAILED,"mmap");
//   // send(netfd,p,statbuf.st_size,MSG_NOSIGNAL);
//   // munmap(p,statbuf.st_size);
//    sendfile(netfd,fd,NULL,statbuf.st_size);
//    close(fd);
//    return 0;
//}
