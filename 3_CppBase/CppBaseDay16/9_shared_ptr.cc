#include <memory>
#include <iostream>
using std::cout;
using std::endl;
using std::shared_ptr;

//shared_ptr循环引用的问题，会导致内存泄漏

class Child;//类的前向声明

class Parent
{
public:
    Parent(){cout << "Parent()" << endl;    }
    ~Parent(){cout << "~Parent()" << endl;    }

    shared_ptr<Child> _pChild;
};

class Child
{
public:
    Child(){cout << "Child()" << endl;    }
    ~Child(){cout << "~Child()" << endl;    }

    shared_ptr<Parent>  _pParent;
};

void test0()
{
    shared_ptr<Parent> spParent(new Parent);
    shared_ptr<Child> spChild(new Child);
    cout << "spParent.use_count():" << spParent.use_count() << endl;
    cout << "spChild.use_count():" << spChild.use_count() << endl;

    spParent->_pChild = spChild;//引用计数加1
    spChild->_pParent = spParent;//引用计数加1
    cout << "spParent.use_count():" << spParent.use_count() << endl;
    cout << "spChild.use_count():" << spChild.use_count() << endl;

}


int main()
{
    test0();
    return 0;
}

