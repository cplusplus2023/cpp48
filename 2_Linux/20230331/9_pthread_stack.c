#include <48func.h>
void *threadFunc(void *arg){
    long * pVal = (long *) arg;
    printf("child, val = %ld\n", *pVal);
}
int main()
{
    long val = 1000;
    pthread_t tid1,tid2,tid3;
    pthread_create(&tid1,NULL,threadFunc,(void *)&val);
    ++val;
    pthread_create(&tid2,NULL,threadFunc,(void *)&val);
    ++val;
    pthread_create(&tid3,NULL,threadFunc,(void *)&val);
    sleep(1);
    printf("main, val = %ld\n", val);
    return 0;
}

