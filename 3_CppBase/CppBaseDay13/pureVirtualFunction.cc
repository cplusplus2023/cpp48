 ///
 /// @file    pureVirtualFunction.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-05 10:21:11
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class A {
public:
	//定义了纯虚函数的类称为抽象类
	//抽象类不能实例化对象
	virtual void print() = 0;
	virtual void display() = 0;
};

class B
: public A
{
public:
	//B只实现了一个纯虚函数，
	//还有一个没有实现，因此也是抽象类
	void print() override
	{
		cout << "B::print()" << endl;
	}
};

class C
: public B
{
public:
	void print() {	cout << "C::print()" << endl;	}
	//当基类中有多个纯虚函数时，派生类必须要将
	//所有的纯虚函数全部实现，该类才不是抽象类
	void display()
	{	cout << "C::display()" << endl;	}
};
 
void test0() 
{
	//A a;
	//B b;
	C c;
	A * pa = &c;
	pa->print();
	pa->display();

	A & ref = c;
	ref.print();
	ref.display();
} 
 
int main(void)
{
	test0();
	return 0;
}
