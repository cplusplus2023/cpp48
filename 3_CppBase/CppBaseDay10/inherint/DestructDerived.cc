 ///
 /// @file    Derived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 16:22:46
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
	A(int ia = 0): _ia(ia) {	cout << "A()" << endl;	}
	~A() {	cout << "~A()" << endl;	}

	long _ia;
};


class Base{
public:
	Base(int ibase = 0)
	: _ibase(ibase)
	{	cout << "Base(int=0)" << endl;	}
	
	void print() const
	{	cout << "Base::_ibase = " << _ibase << endl;	}

	~Base() {	cout << "~Base()" << endl;	}

private:
	long _ibase;
};

class Derived
: public Base
{
public:
	Derived()
	: Base() //即使是无参构造函数，也显式给出来
	, _iderived(0)
	{	cout << "Derived()" << endl;	}

	Derived(int ibase1, int ibase2, int iderived)
	: Base(ibase1) //显式调用基类相应的构造函数
	, _a(ibase2)//对象成员的初始化
	, _iderived(iderived)
	{	cout << "Derived(int)" << endl;	}

	~Derived() {	cout << "~Derived()" << endl;	}
	
	void display() const 
	{
		cout << "Derived::_iderived = " << _iderived << endl;
	}

private:
	A _a;
	long _iderived;
};
 
void test0() 
{
	/* A a1, a2; */
	/* cout << "&a1:" << &a1 << endl; */
	/* cout << "&a2:" << &a2 << endl; */
	/* cout << "sizeof(A):" << sizeof(A) << endl; */
	Derived d1(10, 20, 30);
	d1.print();
	d1.display();
} 
 
int main(void)
{
	test0();
	return 0;
}
