#include <iostream>
#include <vector>
#include <deque>
#include <list>

using std::cout;
using std::endl;
using std::vector;
using std::deque;
using std::list;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

struct CompareList
{
    bool operator()(const int &lhs, const int &rhs) const
    {
        cout << "bool CompareList::operator()" << endl;
        return lhs < rhs;
    }
};

void test()
{
    list<int> number = {1, 3, 3, 5, 7, 3, 9, 8, 6, 4, 2, 5};
    display(number);

    cout << endl << "list的reverse函数" << endl;
    number.reverse();
    display(number);

    cout << endl << "list的unique函数" << endl;
    number.unique();
    display(number);

    cout << endl << "list的sort函数" << endl;
    number.sort();
    /* number.sort(std::less<int>()); */
    /* number.sort(std::greater<int>()); */
    /* number.sort(CompareList()); */
    display(number);

    cout << endl << "list的unique函数" << endl;
    number.unique();
    display(number);

    cout << endl << "list中的merge函数" << endl;
    list<int> number2 = {11, 77, 44, 55, 0};
    number2.sort();
    /* number.sort(std::greater<int>()); */
    /* number2.sort(std::greater<int>()); */
    /* display(number); */
    /* display(number2); */
    number.merge(number2);
    display(number);
    display(number2);

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

