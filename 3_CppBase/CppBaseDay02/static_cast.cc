 ///
 /// @file    static_cast.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 14:34:30
 ///
 
#include <iostream>
using std::cout;
using std::endl;
 
void test0() 
{
	cout << "sizeof(double):" << sizeof(double) << endl;
	double d1 = 11.11;

	int i1 = d1;//隐式转换
	cout << "i1:" << i1 << endl;

	int i2 = 100;//隐式转换
	double d2 = i2;
	cout << "d2:" << d2 << endl;

	int i3 = static_cast<int>(d1);
	cout << "i3:" << i3 << endl;

	//在void * 和其他类型的指针之间转换
	int * p = static_cast<int*>(malloc(sizeof(int)));
	*p = 10;
	cout << "p:" << p << endl;
	cout << "*p :" << *p << endl;

	//一定是安全的
	void *p2 = static_cast<void*>(p);
	cout << "p2:" << p2 << endl;
 
	//是不安全的转换
	//说明static_cast无法知道执行的转换是否安全
	double * p3 = static_cast<double*>(malloc(sizeof(int)));
	*p3 = 11.11;

	//提出需求: 在源码中有哪些地方用到了强制转换
	int * p4 = (int*)malloc(sizeof(int));

} 
 
int main(void)
{
	test0();
	return 0;
}
