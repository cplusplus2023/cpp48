#include <48func.h>
int main(int argc, char *argv[])
{
    // ./4_aqiang 1.pipe 2.pipe
    ARGS_CHECK(argc,3);
    int fdw = open(argv[1],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open fdw");
    int fdr = open(argv[2],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    printf("chat established!\n");
    char buf[4096] = {0};
    while(1){
        // 阿强发消息
        memset(buf,0,sizeof(buf));//每次读操作的时候先清空buf
        read(STDIN_FILENO,buf,sizeof(buf));
        write(fdw,buf,strlen(buf));//键盘输入了一些文本数据
        // 收管道的消息
        memset(buf,0,sizeof(buf));
        read(fdr,buf,sizeof(buf));
        printf("buf = %s\n", buf);
    }
    close(fdr);
    close(fdw);
    return 0;
}

