 ///
 /// @file    anonymousNamespace.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 11:18:17
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//匿名命名空间
namespace
{
//匿名命名空间中的实体只能在本模块内部使用
int num = 1;

}

//有名命名空间中的实体
namespace wd
{
int num = 2;
}//end of namespace wd

//注意：匿名命名空间的实体不能与全局作用域中的实体名相同
//如果相同，就会出现bug
//全局作用域
int gnum = 3;

 
void test0() 
{
	cout << "::num = " << ::num << endl;
	cout << "wd::num = " << wd::num << endl;
} 
