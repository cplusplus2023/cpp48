#include <48func.h>
int main(int argc, char *argv[])
{
    // ./2_pipe_write 1.pipe
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1],O_WRONLY);//打开管道的写端
    ERROR_CHECK(fdw,-1,"open");
    printf("write side is opened\n");
    sleep(1);
    ssize_t sret = write(fdw,"helloworld",10);
    ERROR_CHECK(sret,-1,"write");
    printf("sret = %ld\n",sret);
    sleep(5);
    //printf("sleep over!\n");
    close(fdw);
    return 0;
}

