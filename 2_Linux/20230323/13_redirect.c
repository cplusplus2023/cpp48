#include <48func.h>
int main(int argc, char *argv[])
{
    // ./13_redirect file1
    ARGS_CHECK(argc,2);
    printf("You can see me!\n");//1号文件描述符引用屏幕
    // 在close之前先printf一个换行
    close(STDOUT_FILENO);//关闭了1号 此时最小可用的fd是1
    int fd = open(argv[1],O_RDWR);// 磁盘文件文件对象的fd是1
    ERROR_CHECK(fd,-1,"open");
    printf("fd = %d\n", fd);//后续的printf往磁盘文件当中写入
    printf("You can't see me!\n");
    return 0;
}

