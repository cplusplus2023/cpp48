#include <signal.h>
#include <iostream>

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>

using std::cout;
using std::endl;

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    cout << "sighandler is processing" << endl;
    waitGroup.done();
}

void httpCallback(WFHttpTask * httpTask)
{
    cout << "httpCallback is running" << endl;
    //检查错误情况
    auto req = httpTask->get_req();
    auto resp = httpTask->get_resp();
    int state = httpTask->get_state();
    int error = httpTask->get_error();
    switch(state){
    case  WFT_STATE_SYS_ERROR:
        printf("system error: %s\n", strerror(error));break;
    case  WFT_STATE_DNS_ERROR:
        printf("dns error: %s", gai_strerror(state)); break;
    case WFT_STATE_SUCCESS:
        break;
    }
    if(state != WFT_STATE_SUCCESS) {
        printf("occurs error\n");
        return;
    }

}

void test0()
{
    signal(SIGINT, sighandler);

    //1. 创建HTTP的任务
    auto httpTask = WFTaskFactory::create_http_task(
                    "http://www.baidu.com", 0, 0, httpCallback);

    //2. 交给框架去调度执行
    httpTask->start();
    waitGroup.wait();//调用它就会阻塞进程
}


int main()
{
    test0();
    return 0;
}

