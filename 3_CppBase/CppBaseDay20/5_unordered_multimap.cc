#include <iostream>
#include <unordered_map>
#include <vector>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::unordered_multimap;
using std::vector;
using std::pair;
using std::string;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first 
             << "  " << elem.second << endl;
    }
}

void test()
{
    //unordered_multimap的特征
    //1、unordered_multimap中存放的是key-value类型，也就是pair类型
    //key值是不唯一的，可以重复，value值是可以重复的
    //2、key值是没有顺序的
    //3、底层使用哈希
    unordered_multimap<int, string> number = {
        //1、直接使用pair
        pair<int, string>(1, "beijing"),
        pair<int, string>(3, "nanjing"),
        //2、使用大括号的形式
        {6, "hubei"},
        {8, "wuhan"},
        //3、make_pair函数的返回类型就是pair
        make_pair(2, "beijing"),
        make_pair(7, "wuhan"),
        make_pair(1, "beijing")
    };
    display(number);

    cout << endl << "unordered_multimap的查找操作" << endl;
    size_t cnt1 = number.count(1);
    cout << "cnt1 = " << cnt1 << endl;

    unordered_multimap<int, string>::iterator it = number.find(6);
    if(it != number.end())
    {
        cout << "该元素存在unordered_multimap中 "
             << it->first << " " << it->second << endl;
    }
    else
    {
        cout << "该元素不在unordered_multimap中" << endl;
    }

    cout << endl << "unordered_multimap的插入操作" << endl;
    /* number.insert(pair<int, string>(5, "dongjing")); */
    /* number.insert({5, "dongjing"}); */
    number.insert(make_pair(5, "dongjing"));
    display(number);

    cout << endl << endl;
    number.insert({{4, "wuhan"}, {5, "wangdao"}});
    display(number);

#if 0
    cout << endl << "unordered_multimap中的下标操作" << endl;
    cout << "number[3] = " << number[3] << endl;//查找
    cout << "number[15] = " << number[15] << endl;//插入
    display(number);

    cout << endl << endl;
    /* T &operator[](const Key &); */
    number.operator[](15).operator=("wangdao");
    /* number[15] = "wangdao";//修改 */
    number[2] = "wangdao";
    display(number);
#endif

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

