#include <signal.h>
#include <iostream>

#include <workflow/WFFacilities.h>

using std::cout;
using std::endl;

static WFFacilities::WaitGroup waitGroup(2);

void sighandler(int num)
{
    cout << "sighandler is processing" << endl;
    waitGroup.done();
}

void test0()
{
    signal(SIGINT, sighandler);
    waitGroup.wait();
}


int main()
{
    test0();
    return 0;
}

