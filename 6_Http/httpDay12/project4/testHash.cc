#include "hash.h"
#include "token.h"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    Hash hash("./tmp/1.txt");
    cout << hash.sha1() << endl;
}

void test1()
{
    Token token("Jackie", "12345678");
    string str = token.getToken();
    cout << token.getToken()<< endl
         << " token'size " << str.size() << endl;
}

int main()
{
    test1();
    return 0;
}

