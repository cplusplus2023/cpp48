#include <iostream>
using std::cout;
using std::endl;

//在类中定义一个成员函数，
//该成员函数是一个模板形式的

class Point 
{
public:
    Point(double ix = 0, double iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;}

    //成员函数模板
    template <class T>
    T typecast()
    {   return (T)_ix;  }


private:
    double _ix;
    double _iy;
};

void test0()
{
    Point pt(1.1, 22.2);
    cout << pt.typecast<int>() << endl;
}


int main()
{
    test0();
    return 0;
}

