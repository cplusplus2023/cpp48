#include <math.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::unordered_map;
using std::vector;
using std::pair;
using std::string;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first 
             << "  " << elem.second << endl;
    }
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    float getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);

private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
        << " ," << rhs._iy
        << ")";

    return os;
}

void test()
{
    unordered_map<string, Point> number = {
        pair<string, Point>("wuhan", Point(1, 2)),
        pair<string, Point>("beijing", Point(1, 2)),
        {"hubei", Point(1, -2)},
        {"shenzhen", Point(-1, -2)},
        make_pair("wuhan", Point(1, 2)),
        make_pair("tianjin", Point(3, 2)),
    };
    display(number);

    cout << endl << "unordered_map针对自定义类型的下标访问" << endl;
    cout << "number[\"wuhan\"] = " << number["wuhan"]  << endl;
    cout << "number[\"waha\"] = " << number["waha"]  << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

