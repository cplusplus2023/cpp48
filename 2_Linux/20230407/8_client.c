#include <48func.h>
int main(int argc, char *argv[])
{
    // ./1_client 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);//ipv4 tcp
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(atoi(argv[2]));
    // "1234" --> 小端的1234 --> 大端的1234
    serverAddr.sin_addr.s_addr = inet_addr(argv[1]);
    // "192.168.118.128" 字符串 --> 大端的32bit的ip地址
    int ret = connect(sockfd,(struct sockaddr *)&serverAddr,sizeof(serverAddr));
    // sockaddr_in 赋值 --> 取地址 --> 强转 sockaddr* 
    ERROR_CHECK(ret,-1,"connect");
    //sleep(10);
    ssize_t sret = send(sockfd,"niganma",7,0);
    ERROR_CHECK(sret,-1,"send niganma");
    return 0;
}

