 ///
 /// @file    hello.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-12 11:58:01
 ///
 
//C++的头文件没有*.h的后缀, 
//因为C++头文件是用模板实现的
//
//#include <mycppheaer.hpp>
//#include <mycppheaer.hh>
//
//#include <stdio.h>   推荐使用这种形式，与C++区分开
//#include <string.h>
//
//#include <cstring>
//#include <cstdio>

#include <iostream>
using namespace std;//命名空间
 
int main(int argc, char ** argv)
{
	// << 输出流运算符
	cout << "hello,world" << endl;
	return 0;
}
