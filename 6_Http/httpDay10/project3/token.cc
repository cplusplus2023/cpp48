#include "token.h"
#include <time.h>
#include <stdlib.h>
#include <openssl/md5.h>

#include <iostream>
using std::cout;
using std::endl;


string Token::getToken() const
{
    char timestamp[20] = {0};
    time_t t = time(nullptr);
    struct tm * ptm = localtime(&t);
    sprintf(timestamp, "%04d%02d%02d%02d%02d%02d",
            ptm->tm_year + 1900,
            ptm->tm_mon + 1,
            ptm->tm_mday,
            ptm->tm_hour,
            ptm->tm_min,
            ptm->tm_sec);

    string str = _username + _salt;

    char md[16] = {0};
    MD5((const unsigned char *)str.c_str(), str.size(), (unsigned char*)md);
    char frag[3] = {0};
    string toGenstr;
    for(int i = 0; i < 16; ++i) {
        sprintf(frag, "%02x", md[i]);
        toGenstr += frag;
    }
    return toGenstr + timestamp;
}

