#include <48func.h>
int main(int argc, char *argv[])
{
    // ./3_pipe_read 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);//打开管道的读端
    ERROR_CHECK(fdr,-1,"open");
    printf("read side is opened\n");
//    char buf[1024] = {0};
//    ssize_t sret = read(fdr,buf,sizeof(buf)-1);
//    ERROR_CHECK(sret,-1,"read");
//    printf("sret = %ld, buf = %s\n",sret, buf);
//    sret = read(fdr,buf,sizeof(buf)-1);
//    ERROR_CHECK(sret,-1,"read");
//    printf("sret = %ld, buf = %s\n",sret, buf);
    close(fdr);
    return 0;
}

