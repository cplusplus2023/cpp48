#include <48func.h>
int main(int argc, char *argv[])
{
   // // ./4_azhen 2.pipe 1.pipe
   // ARGS_CHECK(argc,3);
   // int fdw = open(argv[1],O_WRONLY);
   // ERROR_CHECK(fdw,-1,"open fdw");
   // int fdr = open(argv[2],O_RDONLY);
   // ERROR_CHECK(fdr,-1,"open fdr");
    // ./4_azhen 1.pipe 2.pipe
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open fdw");
    printf("chat established!\n");
    char buf[4096] = {0};
    fd_set rdset;//创建监听集合
    FD_ZERO(&rdset);//初始化监听集合
    FD_SET(STDIN_FILENO,&rdset);//监听stdin
    FD_SET(fdr,&rdset);//监听2.pipe管道的读端
    select(fdr+1,&rdset,NULL,NULL,NULL);
    // 第一个参数 监听集合中最大的fd再+1
    // 第二个参数 由于读阻塞的监听集合
    // 第三、四个参数 由于写阻塞和异常阻塞的监听集合
    // 第五个参数 最长等待时间 NULL表示永久等待
    // 当select就绪了以后，rdset就成为了就绪集合
    // 检查某个具体的文件描述符是否在就绪集合当中
    if(FD_ISSET(STDIN_FILENO,&rdset)){
        // 说明stdin有数据
        memset(buf,0,sizeof(buf));
        read(STDIN_FILENO,buf,sizeof(buf));
        write(fdw,buf,strlen(buf));
    }
    if(FD_ISSET(fdr,&rdset)){
        // 说明管道有数据
        memset(buf,0,sizeof(buf));
        read(fdr,buf,sizeof(buf));
        printf("buf = %s\n", buf);
    }
    close(fdw);
    close(fdr);
    return 0;
}

