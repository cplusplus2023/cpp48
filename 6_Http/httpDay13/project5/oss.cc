
#include "oss.h"
#include <iostream>

#include <alibabacloud/oss/OssClient.h>
using std::cout;
using std::endl;


using namespace AlibabaCloud::OSS;

OssUploader::OssUploader(const OssInfo & info)
: _ossinfo(info)
, _conf()
, _ossclient(_ossinfo.endpoint, _ossinfo.accessKeyId, _ossinfo.accessKeySecret, _conf)
{
    InitializeSdk();
}

void OssUploader::doUpload(const string & key, const string & filename)
{
    /* ClientConfiguration conf; */
    /* OssClient client(_ossinfo.endpoint, _ossinfo.accessKeyId, _ossinfo.accessKeySecret, conf); */
    auto outcome = _ossclient.PutObject(_ossinfo.bucketname, key, filename);
    if(!outcome.isSuccess()) {
        cout << outcome.error().Code() 
             << ", " << outcome.error().Message()
             << ", " << outcome.error().RequestId() << endl;
    } else {
        cout << "upload file " << filename << " sucess!" << endl;
    }
}

OssUploader::~OssUploader()
{
    ShutdownSdk();
}

string OssUploader::genreateDownloadUrl(const string & key)
{
    /* ClientConfiguration conf; */
    /* OssClient client(_ossinfo.endpoint, _ossinfo.accessKeyId, _ossinfo.accessKeySecret, conf); */
    time_t t = time(nullptr) + 1200;
    auto outcome = _ossclient.GeneratePresignedUrl(_ossinfo.bucketname, key, t, Http::Get);
    if(!outcome.isSuccess()) {
        cout << outcome.error().Code() 
             << ", " << outcome.error().Message()
             << ", " << outcome.error().RequestId() << endl;
        return string();
    }

    return outcome.result();
}
