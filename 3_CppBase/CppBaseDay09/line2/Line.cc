 ///
 /// @file    typeCast1.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 10:19:24
 ///
 
#include "Line.hpp"

#include <iostream>
using std::cout;
using std::endl;

//在头文件中的前向声明，
//放在cc文件给出具体的实现
class Line::LineImpl
{
	//将Line分成了头文件和实现文件以后，还是能看到
	//Line的内部实现的
	class Point
	{
	public:
		Point(int ix = 0, int iy = 0);

		friend std::ostream & operator<<(std::ostream & os, 
				const Point & rhs);

		~Point() {	cout << "~Point()" << endl;	}

	private:
		int _ix;
		int _iy;
	};
 
public:
	LineImpl(int x1, int y1, int x2, int y2)
	: _pt1(x1, y1)
	, _pt2(x2, y2)
	{	cout << "LineImpl(int,int,int,int)" << endl;	}

	void printLine() const;
	
	friend std::ostream & operator<<(std::ostream & os, const Point & rhs);

private:
	Point _pt1;
	Point _pt2;
};

Line::LineImpl::Point::Point(int ix, int iy)
: _ix(ix)
, _iy(iy)
{
	cout << "Point(int,int)" << endl;
}

void Line::LineImpl::printLine() const
{
	cout << _pt1 << " --> " << _pt2 << endl;
}

std::ostream & operator<<(std::ostream & os, const Line::LineImpl::Point & rhs)
{
	os << "(" << rhs._ix
	   << "," << rhs._iy
	   << ")";
	return os;
}

	
Line::Line(int x1, int y1, int x2, int y2)
: _pimpl(new LineImpl(x1, y1, x2, y2))
{
	cout << "Line(int,int,int,int)" << endl;
}
	
void Line::printLine() const
{
	_pimpl->printLine();
}

