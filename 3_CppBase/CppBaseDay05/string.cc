 ///
 /// @file    string.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 10:18:51
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;
using std::string;
 
void test0() 
{
	string s1;
	cout << "s1:" << s1 << endl;

	//将C风格的字符串转换为C++风格字符串
	string s2("hello,world");
	cout << "s2:" << s2 << endl;

	string s3(5, 'a');
	cout << "s3:" << s3 << endl;

	//获取字符串的长度
	cout << " s2.size : " << s2.size() << endl;
	cout << " s2.length:" << s2.length() << endl;
	//字符串的遍历
	for(size_t i = 0; i < s2.size(); ++i) {
		//通过下标访问任意一个字符
		cout << s2[i] << " ";
	}
	cout << endl;
	
	//遍历，C++11之后引入：增强for循环
	//auto表示的是自动进行推导类型
	//: 是一个分隔符
	for(auto & ch : s2) {
		cout << ch << " ";
	}
	cout << endl;

	//字符串的拼接操作
	string s4 = s2 + s3;
	cout << "s4:" << s4 << endl;

	string s5 = s2 + "wangdao";
	cout << "s5:" << s5 << endl;

	s2 += s3;
	cout << "s2:" << s2 << endl;

	s5.append("bbb");
	cout << "s5:" << s5 << endl;

	//将C++风格的字符串转换为C风格字符串
	const char * pstr = s2.c_str();//只读属性
	cout << "pstr:" << pstr << endl;
	//*pstr = 'H';

	//截取子串
	string s6 = s2.substr(0, 5);
	cout << "s6:" << s6 << endl;

	//string的对象首地址 与字符串内容的首地址是有区别的
	cout << "&s6:" << &s6 << endl;//string * 
	printf("s6's content addr: %p\n", s6.c_str());
	cout << "s6's content:" << s6.c_str() << endl;

	char buff [100] = {0};
	strcpy(buff, s6.c_str());
} 
 
int main(void)
{
	test0();
	return 0;
}
