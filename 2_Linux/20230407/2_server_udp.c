#include <48func.h>
int main(int argc, char *argv[])
{
    // ./2_server_udp 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);// 创建一个udp的socket
    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    int ret = bind(sockfd,(struct sockaddr *)&serveraddr,sizeof(serveraddr));
    ERROR_CHECK(ret,-1,"bind");
    struct sockaddr_in clientaddr;
    socklen_t socklen = sizeof(clientaddr);//socklen必须初始化
    char buf[1024] = {0};
    recvfrom(sockfd,buf,sizeof(buf),0,
             (struct sockaddr *)&clientaddr,&socklen);
    printf("client ip = %s, port = %d\n",
           inet_ntoa(clientaddr.sin_addr),
           ntohs(clientaddr.sin_port));
    printf("buf = %s\n",buf);
    sendto(sockfd,"hello",5,0,
           (struct sockaddr *)&clientaddr,socklen);
    sendto(sockfd,"world",5,0,
           (struct sockaddr *)&clientaddr,socklen);
   // recvfrom(sockfd,buf,sizeof(buf),0,
   //          (struct sockaddr *)&clientaddr,&socklen);
   // printf("client ip = %s, port = %d\n",
   //        inet_ntoa(clientaddr.sin_addr),
   //        ntohs(clientaddr.sin_port));
   // printf("buf = %s\n",buf);
    close(sockfd);
    return 0;
}

