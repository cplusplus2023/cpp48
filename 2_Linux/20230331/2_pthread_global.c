#include <48func.h>
int global = 0;
void *threadFunc(void *arg){
    printf("I am child thread!\n"); 
    for(int i = 0; i < 1000000000; ++i){
        ++global;
    }
    printf("child finish!\n");
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    printf("I am main thread!\n");
    for(int i = 0; i < 1000000000; ++i){
        ++global;
    }
    printf("main finish!\n");
    sleep(3);
    printf("global = %d\n", global);
    return 0;
}

