#include <memory>
#include <iostream>
using std::cout;
using std::endl;
using std::unique_ptr;
using std::shared_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    ~Point() {  cout << "~Point()" << endl; }

    Point * addPoint(Point & pt)
    {
        this->_ix += pt._ix;
        this->_iy += pt._iy;
        return this;
    }

    void print() const
    {   cout << "(" << _ix << "," << _iy << ")" << endl;}

private:
    int _ix;
    int _iy;
};

void test0()
{
    //误用情况: 将一个堆空间的原生裸指针交给不同的智能指针进行托管
    Point * pt = new Point(11, 12);
    unique_ptr<Point> up(pt);
    unique_ptr<Point> up2(pt);
    
}

void test1()
{
    Point * pt = new Point(11, 12);
    shared_ptr<Point> sp(pt);
    /* shared_ptr<Point> sp2(pt); */
    unique_ptr<Point> up(pt);
}

void test2()
{
    //Point * pt = new Point(11, 12);
    unique_ptr<Point> up(new Point(1, 2));
    unique_ptr<Point> up2(new Point(3, 4));
    cout << "up->print():"<< endl;
    up->print();
    cout << "up2->print():"<< endl;
    up2->print();

    cout << "\n调用完reset方法之后:" << endl;
    //对象(3,4)交给up2进行托管
    //up调用reset方法之后，通过up2.get()方法
    //把(3,4)同时又交给了up进行托管
    up.reset(up2.get());
    cout << "up->print():"<< endl;
    up->print();
}

void test3()
{
    //一般情况下，将一个堆对象交给一个智能指针进行托管之后，
    //接下来所有的操作都要交给智能指针完成，这样可以尽量出现误用情况
    shared_ptr<Point> sp(new Point(1, 2));
    shared_ptr<Point> sp2(new Point(11, 12));

    //sp与sp托管的对象都是同一个对象,造成误用
    shared_ptr<Point> sp3(sp->addPoint(*sp2));

    cout << "sp.use_count(): " << sp.use_count() << endl;
    cout << "sp2.use_count(): " << sp2.use_count() << endl;
    cout << "sp3.use_count(): " << sp3.use_count() << endl;
}


int main()
{
    /* test0(); */
    /* test1(); */
    /* test2(); */
    test3();
    return 0;
}

