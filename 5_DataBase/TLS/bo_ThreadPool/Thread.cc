#include "Thread.h"
#include <stdio.h>

__thread const char *name = "thread_name";

Thread::Thread(ThreadCallback &&cb, const string &name)
: _thid(0)
, _name(name)
, _isRunning(false)
, _cb(std::move(cb))
{
}

Thread::~Thread()
{
    if(_isRunning)
    {
        pthread_detach(_thid);
        _isRunning = false;
    }
}

void Thread::start()//this
{
    int ret = pthread_create(&_thid, nullptr, threadFunc, this);
    if(ret)
    {
        perror("pthread_create");
        return;
    }

    _isRunning = true;
}

void Thread::join()
{
    if(_isRunning)
    {
        pthread_join(_thid, nullptr);
        _isRunning = false;
    }
}

void *Thread::threadFunc(void *arg)
{
    Thread *pth = static_cast<Thread *>(arg);
    name = pth->_name.c_str();//给线程局部变量赋值
    if(pth)
    {
        pth->_cb();//回调函数
    }

    return nullptr;
    /* pthread_exit(nullptr); */
}
