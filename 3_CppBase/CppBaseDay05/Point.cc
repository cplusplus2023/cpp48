 ///
 /// @file    Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 10:56:22
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:

#if 1
	Point()
	: _ix(0)
	, _iy(0)
	{	cout << "Point()" << endl;}
#endif


	Point(int ix, int iy)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Point(int,int)" << endl;	}

	~Point()
	{
		cout << "~Point()" << endl;
	}

	void print() const 
	{
		cout << "(" << _ix 
			 << "," << _iy
			 << ")" << endl;
	}

private:
	int _ix;
	int _iy;

};

void test0()
{
	//对象数组会自动调用相应的构造函数
	Point pt[5] = {
		Point(1, 2),
		Point(3, 4)
	};
}

int main(void)
{
	test0();
	return 0;
}
