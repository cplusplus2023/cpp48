#include <iostream>
using std::cout;
using std::endl;

//模板参数的类型有两种：
//1. 类型参数
//2. 非类型参数,即常量表达式,整型数据bool/char/short/int/long
template <class T = int, int kBase = 10>
T multiply(T x, T y)
{
    return x * y * kBase;
}


//非类型参数相比于宏定义和const常量而言，有什么区别呢？
//答: 宏定义和const常量都是编译完毕之后，就已经确定下来了，
//后续在使用过程中，不能进行修改了
//
//而非类型参数可以直接实际传递参数时，动态给出，灵活性大大增加
//

void test0()
{
    int a = 2, b = 5;
    //显式给出非类型参数
    cout << multiply<int, 10>(a, b) << endl;
    cout << multiply<int, 8>(a, b) << endl;
    cout << multiply<int, 16>(a, b) << endl;
    //设置默认参数之后，可以使用默认的
    cout << multiply(a, b) << endl;
}


int main()
{
    test0();
    return 0;
}

