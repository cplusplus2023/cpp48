#include <48func.h>
void *threadFunc(void *arg){
    char *pHeap = (char *)arg; // 原来是什么类型就恢复成什么类型
    printf("pHeap = %s\n", pHeap);
    strcpy(pHeap,"howoldareyou");
}
int main(int argc, char *argv[])
{
    char * pHeap = (char *)malloc(8192);
    strcpy(pHeap,"howareyou");
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,pHeap);
    sleep(1);
    printf("main pHeap = %s\n", pHeap);
    return 0;
}

