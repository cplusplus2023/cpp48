#include <48func.h>
int main()
{
    pthread_mutex_t mutex;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);//初始化锁的属性
    pthread_mutexattr_settype(&attr,PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&mutex,&attr);//init设置锁的属性
    int ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"lock 1");
    printf("lock 1\n");
    ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"lock 2");
    printf("lock 2\n");
    pthread_mutex_unlock(&mutex);
    pthread_mutex_unlock(&mutex);
    pthread_mutexattr_destroy(&attr);
    pthread_mutex_destroy(&mutex);
    return 0;
}

