#include "EventFd.h"
#include "Thread.h"
#include <unistd.h>
#include <iostream>


using std::cout;
using std::endl;

class MyTask
{
public:
    void process()
    {
        cout << "MyTask is running" << endl;
        sleep(1);
    }
};

int main(int argc, char **argv)
{
    MyTask task;
    EventFd ef(std::bind(&MyTask::process, &task));

    Thread th(std::bind(&EventFd::start, &ef));
    th.start();//执行了hanleRead

    int cnt = 20;
    while(cnt-- > 0)
    {
        //执行相应的写操作
        ef.wakeup();
        cout << "cnt = " << cnt << endl;
        sleep(1);
    }

    ef.stop();
    th.join();
    return 0;
}

