 ///
 /// @file    9_constPoint.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 17:23:38
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	, _ref(_ix)
	{
		_ref = _ix;//error, 修改引用成员的值,不是初始化
	}

	void print()
	{
		cout << "(" << _ix
			 << "," << _iy 
			 << ")" << endl;
	}

private:
	//引用成员必须要放在初始化表达式中进行初始化
	int _ix;
	int _iy;
	int & _ref;
};
 
void test0() 
{
	Point p(1, 2);
	p.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
