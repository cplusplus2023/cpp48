 ///
 /// @file    namespace4.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 11:43:08
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//命名空间的命名方式要用小写
namespace wd
{

//函数声明
void display();


//命名空间的嵌套使用
namespace lwh
{
void display()
{
	cout << "wd::lwh::display()" << endl;
}
}//end of namespace lwh

}//end of namespace wd

 
void test0() 
{
	wd::display();
	wd::lwh::display();
} 
 
int main(void)
{
	test0();
	return 0;
}

namespace wd
{
//函数定义
void display() 
{
	cout << "wd::display()" << endl;
}
}//end of namespace wd
