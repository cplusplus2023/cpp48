 ///
 /// @file    Mylogger.hpp
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-26 10:53:15
 ///
 
#ifndef __WD_MYLOGGER_HPP_
#define __WD_MYLOGGER_HPP_

#include <log4cpp/Category.hh>
using std::string;


namespace wd
{

class Mylogger
{
public:
	static Mylogger * getInstance()
	{
		if(nullptr == _pInstance) {
			_pInstance = new Mylogger();
		}
		return _pInstance;
	}

	static void destroy()
	{
		if(_pInstance) {
			delete _pInstance;
			_pInstance = nullptr;
		}
	}

	void error(const char * msg);
	void warn(const char * msg);
	void debug(const char * msg);
	void info(const char * msg);

private:
	Mylogger();
	~Mylogger();

private:
	static Mylogger * _pInstance;
	log4cpp::Category & _mycat;
};

inline
string int2str(int number)
{
	std::ostringstream oss;
	oss << number;
	return oss.str();
}

#define addprefix(msg)	string("[").append(__FILE__)\
		  .append(":").append(__FUNCTION__)\
		  .append(":").append(int2str(__LINE__))\
		  .append("] ").append(msg).c_str()

#define LogError(msg) Mylogger::getInstance()->error(addprefix(msg))
#define LogWarn(msg) Mylogger::getInstance()->warn(addprefix(msg))
#define LogInfo(msg) Mylogger::getInstance()->info(addprefix(msg))
#define LogDebug(msg) Mylogger::getInstance()->debug(addprefix(msg))


}//end of namespace wd
 
 
 
#endif
