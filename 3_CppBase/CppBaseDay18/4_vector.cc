#include <iostream>
#include <vector>
#include <deque>
#include <list>

using std::cout;
using std::endl;
using std::vector;
using std::deque;
using std::list;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(in = 0, int = 0)" << endl;
    }

private:
    int _ix;
    int _iy;

};

void test00()
{
    vector<Point> number;
    /* Point pt(1, 2); */
    /* number.push_back(Point(1, 2)); */
    /* number.push_back(pt); */
    number.emplace_back(1, 2);
}

void test()
{
    cout << "sizeof(vector<int>) = " << sizeof(vector<vector<int>>) << endl;
    cout << "sizeof(vector<long>) = " << sizeof(vector<vector<long>>) << endl;

    cout <<endl;
    vector<int> number = {1, 3, 5, 7, 9, 8, 6, 4, 2};
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector的尾部进行插入与删除" << endl;
    number.push_back(13);
    number.push_back(20);
    display(number);
    number.pop_back();
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
    
    //Q：为何不提供在vector头部进行插入与删除的函数呢？
    //A:vector是一端开口的，元素之间是连续的。如果在头部进行
    //插入与删除的话，会让后面的元素都进行挪动，时间复杂度O(N)
    
    //Q：获取vector中的第一个元素的地址？
    /* &number;//error, 不能取到第一个元素的地址 */
    &number[0];//ok,可以取到第一个元素的地址
    &*number.begin();//ok，可以取到第一个元素的地址
    int *pdata = number.data();//ok，可以获取第一个元素的地址

    //在vector进行insert过程中，可能导致底层已经发生了
    //扩容操作，那么迭代器就有可能失效（任然还指向老
    //的空间），那么要解决这个问题，每次在使用迭代器
    //的时候，都将迭代器重新置位即可。
    cout << endl << "在vector的任意位置进行插入" << endl;
    /* vector<int>::iterator it = number.begin(); */
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 39);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 5, 300);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    vector<int> vec = {111, 333, 666, 888, 222};
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it,  {12, 34, 56, 78});
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "vector中清空元素" << endl;
    number.clear();
    number.shrink_to_fit();//缩减没有使用的内存
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
}

int main(int argc, char **argv)
{
    test00();
    return 0;
}

