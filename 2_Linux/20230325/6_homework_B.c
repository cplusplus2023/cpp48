#include <48func.h>
int main(int argc, char *argv[])
{
    // ./6_homework_B 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdw");
    //int fd= open(argv[2],O_RDWR);
    //ERROR_CHECK(fd,-1,"open fd");
    int size;
    char buf[1024] = {0};
    read(fdr,&size,sizeof(size));
    read(fdr,buf,size);
    printf("buf = %s\n", buf);
    memset(buf,0,sizeof(buf));
    read(fdr,&size,sizeof(size));
    read(fdr,buf,size);
    printf("buf = %s\n", buf);
    return 0;
}

