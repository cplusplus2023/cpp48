#ifndef __THREAD_H__
#define __THREAD_H__

#include <pthread.h>
#include <functional>
#include <string>

using std::function;
using std::bind;
using std::string;

extern __thread const char *name;

using ThreadCallback = function<void()>;

class Thread
{
public:
    Thread(ThreadCallback &&cb, const string &name = string());
    ~Thread();
    void start();
    void join();

private:
    //线程入口函数
    static void *threadFunc(void *arg);

private:
    pthread_t _thid;
    string _name;
    bool _isRunning;
    ThreadCallback _cb;
};

#endif
