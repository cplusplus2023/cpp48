#include "ThreadPool.h"
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

class MyTask
{
public:
    void process() 
    {
        ::srand(clock());;
        int number = ::rand() % 100;
        cout << ">>bo_threadpool.number = " << number
             << ", sub thread name = " << name << endl;
    }

};

int main(int argc, char **argv)
{
    MyTask task;

    ThreadPool threadPool(4, 10);
    threadPool.start();//4个子线程创建并运行

    int cnt = 20;
    while(cnt--> 0)
    {
        threadPool.addTask(std::bind(&MyTask::process, &task));//地址传递
        cout << "cnt = " << cnt << endl;
    }

    threadPool.stop();//五个子线程进行退出

    return 0;
}

