 ///
 /// @file    testAnonymousNamespace.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 11:32:07
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//使用另一个模块的变量gnum
extern int gnum;

//使用另外一个模块中的wd::num
namespace wd
{
extern int num;
}

//当要使用另一个模块中的匿名命名空间中的实体时，
//是拿不到的
namespace {
extern int num;
}


 
void test1() 
{
	cout << "gnum: " << gnum << endl;
	cout << "wd::num : " << wd::num << endl;
	cout << "::num :" << ::num << endl;
} 
 
int main(void)
{
	test1();
	return 0;
}
