 ///
 /// @file    3_Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 10:21:04
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point()
	: _ix(0) 
	, _iy(0)
	{
		cout << "Point()" << endl;
	}

	Point(int x)
	: _iy(x)
	, _ix(_iy)
	{
		cout << "Point(int)" << endl;
	}

	Point(int ix, int iy)
	: _ix(ix)
	, _iy(iy)
	{
		cout << "Point(int,int)" << endl;
	}

	void print() 
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")" << endl;
	}

private:
	int _ix;//初始化的顺序只与在类中被声明的顺序有关
	int _iy;
};

void test1()
{
	Point pt(10);
	cout << "pt:";
	pt.print();
}

 
void test0() 
{
	int a = 1;//初始化
	a = 2;//赋值语句

	int b(3);
	cout << "b: " << b << endl;

	Point pt;
	pt.print();

	Point pt2(1, 2);
	cout << "pt2:";
	pt2.print();
} 
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
