#include "EventFd.h"
#include <unistd.h>
#include <poll.h>
#include <sys/eventfd.h>
#include <iostream>

using std::cerr;
using std::cout;
using std::endl;

EventFd::EventFd(EventFdCallback &&cb)
: _evtfd(createEventFd())
, _isStarted(false)
, _cb(std::move(cb))
{

}

EventFd::~EventFd()
{
    close(_evtfd);
}

void EventFd::start()
{
    struct pollfd pfd;
    pfd.fd = _evtfd;
    pfd.events = POLLIN;

    _isStarted = true;
    while(_isStarted)
    {
        int nready = poll(&pfd, 1, 5000);
        if(-1 == nready && errno == EINTR)
        {
            continue;
        }
        else if(-1 == nready)
        {
            cerr << "-1 == nready " << endl;
            return;
        }
        else if(0 == nready)
        {
            cout << ">>poll_timeout!!!" << endl;
        }
        else
        {
            if(pfd.revents & POLLIN)
            {
                handleRead();//如果执行该函数的线程被唤醒，就可以执行任务
                if(_cb)
                {
                    _cb();//执行了任务
                }
            }
        }
    }
}

void EventFd::stop()
{
    _isStarted = false;
}

void EventFd::handleRead()
{
    uint64_t one = 1;
    int ret = read(_evtfd, &one, sizeof(one));
    if(ret != sizeof(one))
    {
        perror("read");
        return;
    }
}

void EventFd::wakeup()
{
    uint64_t one = 1;
    int ret = write(_evtfd, &one, sizeof(one));
    if(ret != sizeof(one))
    {
        perror("write");
        return;
    }
}

int EventFd::createEventFd()
{
    int fd = eventfd(0, 0);
    if(fd < 0)
    {
        perror("eventfd");
        return fd;
    }
    return fd;
}
