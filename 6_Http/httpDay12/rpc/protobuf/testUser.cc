#include "user.pb.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

void test0()
{
    ReqSignup request;
    request.set_username("admin");
    request.set_password("123");
    //序列化
    string serializStr;
    request.SerializeToString(&serializStr);
    cout << "serializeStr' size: " << serializStr.size() << endl;
    for(size_t i = 0; i < serializStr.size(); ++i) {
        printf("%02x", serializStr[i]);
    }
    printf("\n");
}


int main()
{
    test0();
    return 0;
}

