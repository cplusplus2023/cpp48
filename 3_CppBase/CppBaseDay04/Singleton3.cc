 //
 /// @file    Singleton.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 10:56:22
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//23种设计模式中最简单的，也是高频考点

//单例模式的应用场景：
//1. C语言中的全局变量都可以用单例对象来替换
//2. 配置文件的信息，都可以存储在一个单例对象中
//3. 词典库、网页库都可以用单例对象来存储
//
//
//实现单例模式的步骤：
//1. 将构造函数私有化
//2. 定义了一个静态的成员函数
//3. 定义了一个静态的数据成员

class Singleton
{
private:
	Singleton(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Singleton(int,int)" << endl;	}

	//C++11禁止复制的操作:
	// = delete表示该函数从类中被删除了
	Singleton(const Singleton &) = delete;
	Singleton & operator=(const Singleton&) = delete;

public:
	static Singleton * getInstance()
	{
		//局部静态对象
		static Singleton s1;
		return  &s1;
	}

	void print() const 
	{
		cout << "(" << _ix 
			 << "," << _iy
			 << ")" << endl;
	}

private:
	int _ix;
	int _iy;
};

/* Singleton s1; */
/* static Singleton s2; */
 
void test0() 
{
	//可以生成无数个对象的，
	//因此该语句不能通过编译
	//Singleton pt1(1, 2);//构造函数私有化, 在类之外无法调用构造函数

	//Singleton pt2(3, 4);
	
	Singleton * p1 = Singleton::getInstance();
	p1->print();
	cout << "p1:" << p1 << endl;
 
	Singleton * p2 = Singleton::getInstance();
	p2->print();
	cout << "&p2:" << &p2 << endl;

	Singleton s1 = *p1;

} 
 
int main(void)
{
	test0();
	return 0;
}
