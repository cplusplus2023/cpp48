#include <48func.h>
int main(int argc, char *argv[])
{
   // // ./4_azhen 2.pipe 1.pipe
   // ARGS_CHECK(argc,3);
   // int fdw = open(argv[1],O_WRONLY);
   // ERROR_CHECK(fdw,-1,"open fdw");
   // int fdr = open(argv[2],O_RDONLY);
   // ERROR_CHECK(fdr,-1,"open fdr");
    // ./4_azhen 1.pipe 2.pipe
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open fdw");
    printf("chat established!\n");
    char buf[4096] = {0};
    while(1){
        // 收管道的消息
        memset(buf,0,sizeof(buf));
        read(fdr,buf,sizeof(buf));
        printf("buf = %s\n", buf);
        // 阿珍发消息
        memset(buf,0,sizeof(buf));//每次读操作的时候先清空buf
        read(STDIN_FILENO,buf,sizeof(buf));
        write(fdw,buf,strlen(buf));//键盘输入了一些文本数据
    }
    close(fdw);
    close(fdr);
    return 0;
}

