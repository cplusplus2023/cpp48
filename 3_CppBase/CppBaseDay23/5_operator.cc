#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        
    }
#if 1
    operator  int()
    {
        cout << "operator int()" << endl;
        return _ix + _iy;
    }
#endif

    ~Point()
    {
        
    }
   //没有重载输出流运算符
private:
    int _ix;
    int _iy;
};
    
    
void test()
{
    Point pt(1, 2);
    cout << "pt = " << pt << endl;//pt--->int

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

