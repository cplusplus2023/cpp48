 ///
 /// @file    set.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-03 14:34:35
 ///
 
#include <set>
#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::set;
using std::string;
using std::vector;
 
void test0() 
{
	//set<int> numbers;//元素个数为0
	vector<int> arr{4, 1, 6, 7, 8, 2, 3, 2, 1, 5, 9, 5};

	//set中不能存放关键字相同的元素
	//默认情况下，所有的元素按升序进行排列，以小于符号进行比较
	//
	//底层实现: 红黑树(rbtree)
	//特点:1.节点不是红色就是黑色
	//2. 根节点的颜色是黑色的
	//3. 叶子节点的颜色是黑色的
	//4. 不能有两个连续的节点颜色是红色的
	//5. 从根节点到每一个叶子节点的路径上黑色节点数目相同
	//
	//红黑树近似于一颗平衡二叉树(avl)
	//
	//访问红黑树上的任一节点的时间复杂度为O(logN)
	set<int> numbers(arr.begin(), arr.end());
	cout << "numbers's size:" << numbers.size() << endl;
	for(auto & elem : numbers) {
		cout << elem <<  " ";
	}
	cout << endl;

	//使用迭代器遍历set
	for(auto it = numbers.begin(); it != numbers.end(); ++it) {
		cout << *it << " ";
	}
	cout << endl;
	//set不支持下标访问运算符
	//cout << numbers[0] << endl;
	
	//count操作是当找到元素之后，返回其在容器的数量
	size_t cnt = numbers.count(1);
	cout << "cnt:" << cnt << endl;

	//迭代器类型放在set的public区域，直接可以访问
	set<int>::iterator iter = numbers.find(10);
	if(iter != numbers.end()) {
		cout << "查找成功" << endl;
		cout << *iter << endl;
	} else {
		cout << "查找失败" << endl;
	}
} 

void display(set<int> & numbers)
{
	for(auto & elem : numbers) {
		cout << elem << " ";
	}
	cout << endl;
}

void test1()
{
	std::pair<int, string> p(1, "武汉");
	cout << p.first << " --> " << p.second << endl;

	set<int> numbers{3, 2, 9, 1, 3, 4, 8, 7, 5, 5, 6};
	display(numbers);

	//添加新的元素
	/* auto ret = numbers.insert(10); */
	std::pair<set<int>::iterator, bool> ret = numbers.insert(10);
	if(ret.second) {
		cout << "插入成功" << endl;
	} else {
		cout << "插入失败，容器中已经有了相同的元素" << endl;
	}
	cout << *ret.first << endl;

	//不能修改set中元素的值
	//*ret.first = 11;//error
	
}
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}













