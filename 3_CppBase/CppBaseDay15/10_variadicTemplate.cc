#include <iostream>
using std::cout;
using std::endl;

//必须出现在之前的位置
//该函数是递归调用函数的出口条件
template <class T>
void display(T t)
{   cout << t << endl;  }

//需求: 把可变参数全部输出到终端
template <class T, class... Args>
//void display(Args... args)   //box中有n件物品
void display(T t, Args... args)//box中有n-1件物品， box之外还有1件
{
    cout << t << endl;//先把在box之外的1件物品输出到终端
    //在函数调用时，不能看成一个整体，必须要一件一件传递过去
    //所以是一个解包操作
    display(args...);//再输出n-1件
                    //当...出现在args的右边时，认为是解包操作
}

class Point
{
public:

    friend std::ostream & operator<<(std::ostream &os, const Point & rhs);
private:
    int _ix;
    int _iy;
};

std::ostream & operator<<(std::ostream &os, const Point & rhs)
{
    cout << "(" << rhs._ix
         << "," << rhs._iy
         << ")";
    return os;
}


void test0()
{
    display(1, 2.2, 'a', "hello", Point());
    //  cout << 1 << endl;
    //  display(2.2, 'a', "hello");
    //      cout << 2.2 << endl;
    //      display('a', "hello");
    //          cout << 'a' << endl;
    //              display("hello");
}


int main()
{
    test0();
    return 0;
}

