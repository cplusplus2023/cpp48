 ///
 /// @file    Derived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 16:22:46
 ///
 
#include <iostream>
using std::cout;
using std::endl;


//派生类对象创建的规则：
//2. 当基类有显式定义构造函数时，派生类对象创建时
//如果不希望自动调用基类无参构造函数，则必须要在
//派生类构造函数的初始化表达式中显式调用基类
//相应构造函数，初始化基类部分

class Base{
public:
	Base(int ibase = 0)
	: _ibase(ibase)
	{	cout << "Base(int=0)" << endl;	}
	
	void print() const
	{	cout << "Base::_ibase = " << _ibase << endl;	}

private:
	long _ibase;
};

class Derived
: public Base
{
public:
	Derived()
	: Base() //即使是无参构造函数，也显式给出来
	, _iderived(0)
	{	cout << "Derived()" << endl;	}

	Derived(int ibase, int iderived)
	: Base(ibase) //显式调用基类相应的构造函数
	, _iderived(iderived)
	{	cout << "Derived(int)" << endl;	}
	
	void display() const 
	{
		cout << "Derived::_iderived = " << _iderived << endl;
	}

private:
	long _iderived;
};
 
void test0() 
{
	Derived d1(10, 20);
	d1.print();
	d1.display();
} 
 
int main(void)
{
	test0();
	return 0;
}
