 ///
 /// @file    io4.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 18:04:05
 ///
 
#include <fstream>
#include <sstream>
#include <iostream>
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ostringstream;
using std::istringstream;
 
void test0() 
{
	int num1 = 1;
	int num2 = 2;
	
	ostringstream oss;

	//功能类似于sprintf
	oss << "num1= " << num1 << " num2= " << num2;
	cout << oss.str() << endl;

	num1 = 10;
	num2 = 20;
	cout << "修改num1和num2的值" << endl;

	string line = oss.str();
	string word1, word2;

	//将字符串Line放入输入流中
	istringstream iss(line);
	//前提条件：知道数据的格式
	iss >> word1 >> num1 >> word2 >> num2;
	cout << "num1:" << num1 << endl
		 << "num2:" << num2 << endl;
} 

void readConfig(const string & filename)
{
	ifstream ifs(filename);
	if(!ifs) {
		cout << "ifstream read " << filename << " error" << endl;
		return ;
	}
	
	string line;
	string key, value;
	while(std::getline(ifs, line)) {
		istringstream iss(line);
		iss >> key >> value;
		cout << key << " --> " << value << endl;
	}

	ifs.close();
}

void test1()
{
	readConfig("server.conf");
}
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
