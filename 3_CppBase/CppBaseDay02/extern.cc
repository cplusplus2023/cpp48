 ///
 /// @file    overload.c
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 15:02:56
 ///
 
#include <stdio.h>
#include <iostream>

using std::cout;
using std::endl;


//该函数希望采用C的方式进行调用
//不希望该函数进行名字改编

//__cplusplus该宏只有C++的编译器才会定义
//C的编译器中没有定义该宏
//
#ifdef __cplusplus
extern "C"
{
#endif
//在extern "C" 大括号范围内的代码
//要按C的方式进行调用
int add(int x, int y)
{	return x + y;	}

#ifdef __cplusplus
}//end of extern C
#endif


int add(int x, int y, int z)
{
	return x + y + z;
}

int add(int x, long y)
{	return x + y;	}

int add(long x, int y)
{	return x + y;	}

double add(double x, double y)
{	return x + y;	}
 
int main(int argc, char *argv[])
{
	int i1 = 1, i2 = 2;
	double d1 = 11.11, d2 = 12.12;
	cout << "add(i1, i2):" << add(i1, i2) << endl;
	cout << "add(d1, d2):" << add(d1, d2) << endl;
}
