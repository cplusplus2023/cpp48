 ///
 /// @file    Derived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 16:22:46
 ///
 
#include <iostream>
using std::cout;
using std::endl;


//派生类对象创建的规则：
//1. 当基类没有定义任何构造函数时，派生类对象创建时
//会自动调用基类无参构造函数初始化基类部分

class Base{
public:
	Base(int ibase = 0)
	: _ibase(ibase)
	{	cout << "Base(int=0)" << endl;	}
	
	void print() const
	{	cout << "Base::_ibase = " << _ibase << endl;	}

private:
	long _ibase;
};

class Derived
: public Base
{
public:
	Derived()
	: _iderived(0)
	{	cout << "Derived()" << endl;	}
	
	void display() const 
	{
		cout << "Derived::_iderived = " << _iderived << endl;
	}

private:
	long _iderived;
};
 
void test0() 
{
	Derived d;
	d.print();
	d.display();
} 
 
int main(void)
{
	test0();
	return 0;
}
